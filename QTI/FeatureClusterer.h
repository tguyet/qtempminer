/*
 * IntervalClusterer.h
 *
 *  Created on: Aug 4, 2015
 *      Author: tguyet
 */

#ifndef INTERVALSEQUENCES_INTERVALCLUSTERER_H_
#define INTERVALSEQUENCES_INTERVALCLUSTERER_H_

#include <vector>
#include <map>
#include <list>
#include "sequence.h"


using namespace std;


/**
 * TODO voir si l'ajoute d'une structure de données pour éviter le calcul
 * des distances (map< pair<featureset*,featureset*>, double>) peut améliorer les temps de calcul
 */
class FeatureClusterer {
protected:
	vector<featureset*> _data;
	vector<vector<int> > _clusterid; //!< _clusterid[i] gives the list of the vectors owned by the cluster i
	vector<int> _centroids; //!< id of the centroids in the _data

	const unsigned int max_tries = 5;
public:
	FeatureClusterer();
	virtual ~FeatureClusterer();

	//getters
	const vector<featureset*>& data() const {return _data;};
	const vector<vector<int> >& clusters() const {return _clusterid;};

	/**
	 *
	 */
	const vector<int>& centroids() const {return _centroids;};

	void setData(vector<featureset*>& d);

	//clustering fonctions
	void cluster(unsigned int nbcluster);
	unsigned int cluster_bestk(unsigned int mink, unsigned int maxk);


	void cluster_xpam();

protected:
	int kmedoids(unsigned int nclusters, int maxit, float *bic =nullptr);
	void getclustermedoids();
	void randomassign(unsigned int nclusters);

	float uniform();
	int binomial(int n, float p);
};

#endif /* INTERVALSEQUENCES_INTERVALCLUSTERER_H_ */
