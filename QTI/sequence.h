/*
 *    This file is part of QTIMiner.
 *
 *    SeqStreamMiner is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    SeqStreamMiner is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sequence.h
 * \author Guyet Thomas, AGROCAMPUS-OUEST/IRISA
 * \version 0.3
 * \date aout 2015
 */

#ifndef SEQUENCE_H_INCLUDED
#define SEQUENCE_H_INCLUDED
#include <list>
#include <map>
#include <vector>
#include <ostream>
#include <istream>
#include <iostream>


class pattern;

class sequenceFactory;

using namespace std;

/**
 * items are unsigned integers
 */
typedef unsigned int eventtype;

class feature {

public:
	feature(){};
	feature(const feature &){};
	virtual ~feature(){};
	virtual bool operator==(const feature & f) const =0;
	virtual float operator-(const feature & f) const =0;
	virtual void print(ostream &os =cout) const =0;
};

ostream& operator<< (ostream & os, const feature & is);


class featureset {
protected:
	vector<feature *> _features;
	unsigned int _sid=0;
public:
	featureset(){};
	featureset(feature* fsi);
	featureset(const vector<feature*> &fsi);
	featureset(const featureset& fsi);
	virtual ~featureset(){};

	void setSID(unsigned int id) {_sid=id;};
	unsigned int sid() const {return _sid;};
	unsigned int dim() const {return _features.size();};//return the "dimension of the objects)
	const feature *operator[](unsigned int i) const {return _features[i];};

	virtual float operator-(const featureset &) const =0;
	virtual featureset *clone() const =0;
	virtual void append(feature *);

	friend ostream& operator<< (ostream & os, const featureset & fs);
};

//float operator-(const featureset *, const featureset *);



/**
 * \class event
 * \brief couple id/feature
 * \author Guyet Thomas, AGROCAMPUS-OUEST
 */
class event {
protected:
	eventtype _id;
	feature *_f;
protected:
	/**
	 * \brief Default constructor
	 */
	event():_id(0),_f(nullptr){};
public:

	/**
	 * \brief Copy constructor
	 */
	event(const event &is):_id(is._id),_f(is._f){};

	/**
	 * \brief Constructor with a first item
	 */
	//event(unsigned int i):_id(i),_f(nullptr){};

	/**
	 * \brief Destructor
	 */
	~event(){};

	/**
	 * \brief getter
	 */
	eventtype id() const {return _id;};

	/**
	 * \brief getter
	 */
	const feature & f() const {return *_f;};

	/**
	 * \brief getter
	 */
	feature & f() {return *_f;};

	/**
	 * \brief opérateur de comparaison basé si le timestamp
	 *
	 * Cette fonction est utile pour ordonné les séquences d'itemset.
	 * \see Data::load
	 */
	inline bool operator<(const event& i2) const { return _id < i2._id;};

	/**
	 * \brief Comparison operator
	 */
	int operator==(const event &) const;

	/**
	 * \brief Comparison operator
	 */
	int operator!=(const event &) const;

	/**
	 * \brief stream output (printing)
	 */
	friend ostream& operator<< (ostream & os, const event & is);
	friend sequenceFactory;
};

/**
 * \class signature
 * \brief sequence of eventype
 */
class signature : public vector<eventtype> {};

/**
 * \class stream
 * \brief a stream is an ordered set of itemsets
 */
class sequence : public vector<event>
{
protected:
	int _sid;		//!< id of the sequence in the database

public:
	/**
	 * \brief Default constructor
	 */
	sequence():_sid(0){};

	/**
	 * \brief Destructor
	 */
	~sequence(){};

	/**
	 * \brief getter
	 */
	int sid() const {return _sid;};

	/**
	 * \brief simple function overload
	 */
	unsigned int size() const {return vector<event>::size();};

	/**
	 * \brief print the stream on cout
	 */
	void print(ostream &os = cout) const;


	friend sequenceFactory;

};

ostream& operator<< (ostream & os, const sequence & is);


class patternfeature {
public:
	virtual ~patternfeature(){};

	virtual patternfeature *clone() const =0;

	//projection operator
	virtual const feature & operator[](unsigned int p) const =0;

	virtual void print(ostream& os) const =0;
};

ostream& operator<< (ostream & os, const patternfeature & pf);


class sequenceFactory {
protected:
	char eventdelimiter=',';
public:
	sequenceFactory(){};
	virtual ~sequenceFactory(){};


	virtual event createEvent(istream &str) const;
	event createEvent(const string &str) const;
	sequence createSequence(istream &str) const;
	vector<pair<unsigned int, sequence>> createDatabase(istream &is) const;


	virtual featureset *createFeature(event &) const {return nullptr;};
	virtual featureset *createFeature(list<event*> &) const {return nullptr;};

	virtual pattern * createPattern();
	virtual patternfeature *createPatternFeature(const featureset *center,  list<const featureset*> &instances) const {return nullptr;};

	void setEventDelimiter(char c) {eventdelimiter=c;};
protected:
	event createEvent() const {return event();};
	void setId(event &e, eventtype id) const {e._id=id;};
	void setId(sequence &s, unsigned int id) const {s._sid=id;};
	void setFeature(event &e, feature &id) const {e._f=&id;};
	void setFeature(event &e, feature *id) const {e._f=id;};
};


#endif // STREAM_H_INCLUDED
