#!/usr/bin/python
from prefixspan_module import *

print "test with sequences of itemsets"
data=[  [6],
        [1, 2, 3],
        [ 1,3,5, 2,4],
        [ 1,4, 3,4 ],
        [ 2,3,4, 1, 1,3, 2,4, 4],
        [ 1,3,5 ]]
print "data:", data

pat=QTIPrefixSpan(data, 2)
print pat
#print "patterns (threshold 2):", pat

data2=[  [ (6,1.2,2.3), (2,5.2,5.6)],
        [ (1,0.6,1.2), (2,2.6,2.9), (3,3.6,5.8)],
        [ (1,2.3,3.1),(3,2.9,5.6),(5,4.5,4.9), (2,4.6,5.9),(4,5.6,7.2)],
        [ (2,3.4, 10.2), (1,3.9, 8.2),(4, 4.2,4.5)],
        [ (1,0.2,1.5),(3,1.2,1.8),(5,2.5,5.6) ]]
print "data:", data2

pat=QTIPrefixSpan(data2, 2)
print "pattern:", pat

pat=QTIPrefixSpan(data2, 2, 1)
print "pattern (maxlen):", pat





