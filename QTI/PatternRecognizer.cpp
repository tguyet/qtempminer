/*
 * PatternRecognizer.cpp
 *
 *  Created on: 10 oct. 2015
 *      Author: tguyet
 */

#include <PatternRecognizer.h>
#include "sequence.h"

PatternRecognizer::PatternOccurrence PatternRecognizer::NoOccurrence;

/**
 * Comparison operator between pattern occurrences
 *
 * This operator can be used to compare occurrences or to test if an occurrence is the NoOccurrence
 */
bool operator==(const PatternRecognizer::PatternOccurrence &occ1, const PatternRecognizer::PatternOccurrence &occ2) {
	if( occ1.seqid==-1 ) { //the NoOccurrence occurrence
		return occ2.occ.empty();
	}
	if (occ2.seqid==-1) { //the NoOccurrence occurrence
		return occ1.occ.empty();
	}
	if( occ1.seqid != occ2.seqid ) {
		return 0;
	}
	if( occ1.occ.size() != occ2.occ.size() ) {
		return 0;
	}
	list<unsigned int>::const_iterator it1 = occ1.occ.begin();
	list<unsigned int>::const_iterator it2 = occ2.occ.begin();
	while( it1 !=occ1.occ.end() ) {
		if( *it1 != *it2 ) {
			return 0;
		}
	}
	return 1;
}
bool operator!=(const PatternRecognizer::PatternOccurrence &occ1, const PatternRecognizer::PatternOccurrence &occ2) {
	return not (occ1==occ2);
}
PatternRecognizer::PatternRecognizer(const pattern &pat, double t) : _pat(pat), _threshold(t){}

PatternRecognizer::~PatternRecognizer() {}

//TODO
/*
list<PatternRecognizer::PatternOccurrence> PatternRecognizer::all(sequence &seq) {

}
*/

PatternRecognizer::PatternOccurrence PatternRecognizer::next(sequence &seq) {
	// get the current index of reading this sequence
	int pos;
	sequence_info seqinfos;
	map<sequence *, sequence_info>::iterator it = _current_indexes.find(&seq);
	if( it != _current_indexes.end() ) {
		seqinfos = it->second;
		pos = seqinfos.reading_index;
	} else {
		pos = 0;
		seqinfos.reading_index = 0;
		_current_indexes.insert( make_pair(&seq, seqinfos));
	}

	cout << "\tRecognizer: launched on " << seq.sid() <<" with position " << pos << " (waiting partial occurrences: " << seqinfos.ongoing_recognized.size()<< ")" <<endl;

	//empty the ongoing complete occurrences!
	// This could happen while an event can be used for several finishing event of a pattern
	if ( seqinfos.ongoing_recognized.size()>0 && seqinfos.ongoing_recognized.back().second.occ.size() == _pat.size() ) {
		cout << "Recognizer: last has been recognized" <<endl;
		PatternOccurrence occ = seqinfos.ongoing_recognized.back().second;
		seqinfos.ongoing_recognized.pop_back();
		return occ;
	}
	//then, we can go to see next events
	pos ++;
	//search for the next occurrence
	while( (unsigned int)pos<seq.size() ) {
		//try to complete the existing partial occurrences
		for(auto ongoing : seqinfos.ongoing_recognized) {
			unsigned int patpos = ongoing.first +1;
			if( /*compare events*/_pat.getSignature(patpos) == seq[pos].id() && /*compare features*/ ( (*_pat.getFeature(0)) - seq[pos].f() )<=_threshold ) {
				cout << "\tRecognizer: " << patpos << ":" << pos <<endl;
				ongoing.first++;
				ongoing.second.occ.push_back(pos);
				if(patpos==_pat.size()-1) {
					//the full pattern have been recognized
					_current_indexes[&seq].reading_index = pos;
					PatternOccurrence occ =ongoing.second;
					seqinfos.ongoing_recognized.pop_back(); //it is necessarily the last one that is first recognized !!
					return occ;
				}
			}
		}
		// try create a new partial occurrences
		if( _pat.getSignature(0) == seq[pos].id() ) {
			cout << "\tRecognizer: new start:" << pos <<endl;

			PatternOccurrence occ;
			occ.seqid = seq.sid();
			occ.occ.push_back(pos);
			/*
			intervalFeature &ifs = seq[pos].f();
			intervalFeature &pifs = * _pat.getFeature(0);
			occ.time_decay = ifs.s() - pifs->s();
			*/
			if (_pat.size()== 1 ) { //very small patterns
				//do not include it into the ongoing recognized list
				_current_indexes[&seq].reading_index = pos;
				return occ;
			}
			seqinfos.ongoing_recognized.push_back( make_pair(0,occ) );
		}
		pos++;
	}

	//not any pattern have been recognized in the sequence !
	cout << "** no more occurrences **" <<endl;
	return NoOccurrence;
}


void PatternRecognizer::rewind(sequence &seq) {
	map<sequence*, sequence_info>::iterator it = _current_indexes.find(&seq);
	if( it != _current_indexes.end() ) {
		it->second.reading_index = 0;
		it->second.ongoing_recognized.clear();
	} else {
		sequence_info seqinfos;
		seqinfos.reading_index = 0;
		_current_indexes.insert( make_pair(&seq, seqinfos));
	}
}
