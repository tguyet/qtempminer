
#include "QTI/qtiprefixspan.h"
#include "QTI/sequence.h"
#include "IntervalSequences/IntervalEvent.h"
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

using namespace std;

int main() {
	cout << ">>>>> TEST: reading file <<<<<" <<endl;

	std::ifstream ifs;
	ifs.open ("/home/tguyet/Progs/QTempIntMinerCpp/tests/example.txt", std::ifstream::in);

	if( !ifs ) {
		cerr << "error while opening the file (seems it does not exists !)"<<endl;
		cout << "merde!!" <<endl;
		return 1;
	}

	cout << ">> reading examples <<" <<endl;
	IntervalSequenceFactory factory;

	QTIPrefixspan ps(&factory);
	ps.readdatabase(ifs);
	ifs.close();

	cout << "===== database ===== " << endl;
	for(auto s : ps.database()) {
		cout << s.first << ":" << s.second <<endl;

	}
	cout << "====================" <<endl;

	cout << ">> running prefixspan <<" <<endl;
	ps.show_maximal();
	ps.run(/*fmin*/10, nullptr, /*lenmax*/5);

	return 0;
}
