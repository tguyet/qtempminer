/*
 * Interval.h
 *
 *  Created on: Aug 4, 2015
 *      Author: tguyet
 */

#ifndef INTERVALSEQUENCES_INTERVAL_H_
#define INTERVALSEQUENCES_INTERVAL_H_

#include <vector>
#include <iostream>
#include <cmath>

/**
 * \brief classe de représentation d'un interval
 *
 * Le maximum a été mis en inline pour des raisons de perf !
 */
class Interval {
protected:
	double _l,_u;
public:
	Interval():_l(0),_u(0){};
	Interval(double l, double w):_l(l), _u(w) {};
	Interval(double l):_l(l), _u(l) {};
	virtual ~Interval(){};

	double u() const {return _u;};
	double l() const {return _l;};
	double length() const {return _u-_l;};

	bool is_singleton() const {return _u==_l;};

	bool support(double v) const { return v<=_u && v>=_l; };

	inline void extend(double v)
	{
		if(v>_u) _u=v;
		if(v<_l) _l=v;
	};
	inline void extendleft(double v){if(v<_l) _l=v;};
	inline void extendright(double v){if(v>_u) _u=v;};

	void setL(double val){_l=val;};
	void setU(double val){_u=val;};

	void printcsv(std::ostream& out) const;

	friend std::ostream& operator<<(std::ostream& out, const Interval& I);
};


/**
 * operateur de verification de l'inclusion des intervalles
 */
bool operator<(Interval const &I1, Interval const &I2);
bool operator==(Interval const &I1, Interval const &I2);
bool operator!=(Interval const &I1, Interval const &I2);
bool operator<=(Interval const &I1, Interval const &I2);


class IntervalComparator {
public:
	virtual ~IntervalComparator(){};
	virtual double operator()(const Interval &I,const Interval &I2) =0;
};

class IntervalEuclidiean : public IntervalComparator {
public:
	double operator()(const Interval &I,const Interval &I2) {
		double r=I.u()-I2.u();
		double s=I.l()-I2.l();
		return r*r+s*s;
	};
};

class IntervalCityBlock : public IntervalComparator {
public:
	double operator()(const Interval &I,const Interval &I2) {
		double r=I.u()-I2.u();
		double s=I.l()-I2.l();
		return abs(r)+abs(s);
	};
};

class IntervalHaussdorf : public IntervalComparator {
public:
	double operator()(const Interval &I,const Interval &I2) {
		double r=I.u()-I2.u();
		double s=I.l()-I2.l();
		return abs(r)+abs(s);
	};
};



#endif /* INTERVALSEQUENCES_INTERVAL_H_ */
