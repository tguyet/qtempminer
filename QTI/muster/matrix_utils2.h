/*
 * matrix_utilsé.h
 *
 *  Created on: 2 oct. 2015
 *      Author: tguyet
 */

#ifndef MATRIX_UTILS_H_
#define MATRIX_UTILS_H_

#include <vector>

using namespace std;

namespace cluster {

	/**
	 * \class dissimilarity_matrix
	 * A dissimilarity matrix is a symetric squared matrix: save memory space!
	 */
	class dissimilarity_matrix {

	protected:
		vector< vector<double> > _data;
		double _d=0; //diagnonal value
		unsigned int _size =0;
	public:
		dissimilarity_matrix(unsigned int s) {
			_data.size();
			resize(s,s);
		};

		virtual ~dissimilarity_matrix(){
			_data.clear();
		};

		void resize(unsigned int s1, unsigned int s2) {
			//_data.clear();
			_data.resize(s1);
			for(unsigned int i=1; i<s1; i++) {
				_data[i].resize(i);
			}
			_size =s1;
		};

		unsigned int size1() const {
			return _size;
		};

		unsigned int size2() const {
			return _size;
		};

		double operator()(const unsigned int nRow, const unsigned int nCol) const {
			if( nRow==nCol ) return _d;
			if (nRow > nCol) {
				return _data[nRow][nCol];
			} else {
				return _data[nCol][nRow];
			}
		};

		double& operator()(const unsigned int nRow, const unsigned int nCol) {
			if( nRow==nCol ) return _d;
			if (nRow > nCol) {
				return _data[nRow][nCol];
			} else {
				return _data[nCol][nRow];
			}

		};
	};


	double sum( const dissimilarity_matrix &matrix );
}

#endif /* MATRIX_UTILS_H_ */
