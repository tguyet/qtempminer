/*
 *    This file is part of QTIMiner.
 *
 *    SeqStreamMiner is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    SeqStreamMiner is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with SeqStreamMiner.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sequence.cpp
 * \author Guyet Thomas, AGROCAMPUS-OUEST/IRISA
 * \date aout 2015
 */

#include <iostream>
#include <sstream>
#include "sequence.h"
#include "pattern.h"

int event::operator==(const event & is) const
{
	return (_id == is._id) && (_f == is._f);
}

int event::operator!=(const event & is) const {
	return !operator==(is);
}

/**
 * \brief Global function required to overload the itemset streaming operator
 */
ostream& operator<< (ostream & os, const event & is) {
	os << "(" << is._id;
	if( is._f)
		os << "," << (*is._f);
	os << ")";
	return os;
}

ostream& operator<< (ostream & os, const feature & is) {
	is.print(os);
	return os;
}

///////////////////////////////////////////////////////////////////

/**
 * no recopy of the pointers !
 */
featureset::featureset(const featureset& fsi) {
	_features = fsi._features;
	_sid = fsi._sid;
}

featureset::featureset(feature*fsi) {
	_features.push_back(fsi);
}

featureset::featureset(const vector<feature*> &fsi) {
	_features = fsi; //copy
}

void featureset::append(feature *fs) {
	_features.push_back(fs);
}

ostream& operator<< (ostream & os, const featureset & fs) {
	for(unsigned int i=0; i<fs._features.size()-1;i++) {
		os << *(fs._features[i]) << ",";
	}
	os << *(fs._features[fs._features.size()-1]);
	return os;
}

////////////////////////////////////////////////////////////////


void sequence::print(ostream &os) const {
	vector<event>::const_iterator it=begin();
	os << "[S" << _sid << "]: ";
	while( it!=end() ) {
		os << (*it);
		it++;
		if( it!=end()) {
			os << ", ";
		}
	}
}

ostream& operator<< (ostream & os, const sequence & s) {
	s.print(os);
	return os;
}

////////////////////////////////////////////////////////////////////

ostream& operator<< (ostream & os, const patternfeature & pf) {
	pf.print(os);
	return os;
}

////////////////////////////////////////////////////////////////////

event sequenceFactory::createEvent(istream &str) const {
	event e;
	str >> e._id; //read an integer
	if ( str.fail() ) {
		throw string("reading error: invalid identifier (required unsigned integers)\n");
	}
	if( !str.eof() ) {
		throw string("reading error: delimiter error (expected '")+string(1,eventdelimiter)+string("').\n");
	}
	return e;
}

event sequenceFactory::createEvent(const string &str) const {
	event e;
	stringstream sstr;
	sstr << str;
	return createEvent(sstr);
}

sequence sequenceFactory::createSequence(istream &is) const {
	sequence seq;
	while (is) {
		string s;
		if( !getline(is, s, eventdelimiter ) ) {
			break;
		}
		seq.push_back( createEvent(s) );
	}
	return seq;
}

pattern * sequenceFactory::createPattern() {
	return new pattern();
}

vector<pair<unsigned int, sequence>> sequenceFactory::createDatabase(istream &is) const {
	vector<pair<unsigned int, sequence>> db;
	unsigned int i=0;
	while (is) {
		string s;
		if( !getline( is, s ) ) return db;

		//create the sequence
		istringstream ss( s );
		sequence seq = createSequence(ss);

		//add the sequence into the database
		seq._sid=i;
		db.push_back( make_pair(i, seq) );
		i++;
	}
	if (!is.eof()) {
		cerr << "sequence reading error" <<endl;
	}
	return db;
}

