/**
 PrefiSpan --- An Implementation of Prefix-projected Sequential Pattern mining
           --- A Python Binding

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>, 08/2014

 License: GPL2 (Gnu General Public License Version 2)
*/
#if defined(_WIN32) || defined(WIN32)
#include "Python.h"
#else
#include "python2.7/Python.h"
#endif

#include "QTI/qtiprefixspan.h"
#include "QTI/sequence.h"
#include "IntervalSequences/IntervalEvent.h"
#include "PythonFactory.h"
#include <climits>
#include <list>
#include <vector>

extern "C" {

/*  wrapped prefix-span function */
static PyObject* QTIPrefixSpan(PyObject* self, PyObject* args) {
    int n=0;
    PyObject *list;
    int th=0; //threshold
    int maxlen=0; //threshold
    short int options=0;
    /*  parse the input, from python type to c type */
    if (! PyArg_ParseTuple(args, "O!i|bb", &PyList_Type, &list, &th, &maxlen, &options)) {
        return NULL;
    }
    n = PyList_Size(list);
    if (n < 0)
        return NULL; /* Not a list */
    if(th <=0 )
        return NULL; /* invalid threshold */ 
    
    PythonFactory factory;
	QTIPrefixspan ps(&factory);

	vector<sequence> seqs = factory.createDatabase(list);
	for(sequence seq : seqs) {
		ps.addTransaction(seq);
	}

	/*
	cout << "===== database ===== " << endl;
	for(auto s : ps.database()) {
		cout << s.first << ":" << s.second <<endl;

	}
	cout << "====================" <<endl;


    cout << ">> running prefixspan " << th <<" <<" <<endl;
    */
	ps.show_maximal();

	std::list< pattern* > patlist; //a list to gather the patterns
	ps.run(/*fmin*/th, &patlist, maxlen);
    
    PyObject *result = factory.createPatternsList( patlist );
    //cout << "here"<<endl;
    //Py_INCREF(result);
    return result;
}

/*  define functions in module */
static PyMethodDef PSMethods[] =
{
     {"QTIPrefixSpan", QTIPrefixSpan, METH_VARARGS, "QTIPrefixSpan algorithm"},
     {NULL, NULL, 0, NULL}
};

/* module initialization */
PyMODINIT_FUNC

initprefixspan_module(void)
{
    PyObject *PSModule = Py_InitModule("prefixspan_module", PSMethods);
    PyObject *d = PyModule_GetDict(PSModule);

    //Exemple de definition de constantes
    PyDict_SetItemString(d, "ALL", Py_BuildValue("l",0) );
    PyDict_SetItemString(d, "CLOSED", Py_BuildValue("l",1) );
    PyDict_SetItemString(d, "MAXIMAL", Py_BuildValue("l",2) );
}

} //extern "C"


