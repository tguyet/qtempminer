/*
 * Pattern.h
 *
 *  Created on: Aug 3, 2015
 *      Author: tguyet
 */

#ifndef QTI_PATTERN_H_
#define QTI_PATTERN_H_

#include "sequence.h"

class pattern {
protected:
	vector<eventtype> _base;
	patternfeature *_pf=nullptr;
	unsigned int _support=0;
public:
	pattern();
	pattern(const pattern &);
	virtual ~pattern();

	virtual pattern *clone() const;

	void push_back(eventtype et) {_base.push_back(et);};
	void pop_back() {_base.pop_back();};
	unsigned int size() const {return _base.size();};
	signature getSignature() const;
	eventtype getSignature(unsigned int i) const;

	unsigned int support() const {return _support;};
	void setSupport(unsigned int s) {_support = s;};

	/**
	 * The pattern take the ownership of the object! More especially, the pointer will be deleted by the pattern.
	 */
	void setPatternFeature(patternfeature *pf) {if(_pf) delete(_pf); _pf=pf;};
	const patternfeature *getFeature() const {return _pf;};
	const feature *getFeature(unsigned int i) const;

	friend ostream& operator<<(ostream &os, const pattern &p);

	//TODO projection operator !
};





#endif /* QTI_PATTERN_H_ */
