/**
 PrefiSpan --- An Implementation of Prefix-projected Sequential Pattern mining

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>,
	     adapted from Yasuo Tabei <tabei@cb.k.u-tokyo.ac.jp>

 Date (last update): 03/2015

 License: GPL2 (Gnu General Public License Version 2)

 Reference:
  PrefixSpan: Mining Sequential Patterns Efficiently by Prefix-Projected Pattern Growth
  Jian Pei, Jiawei Han, Behzad Mortazavi-asl, Helen Pinto, Qiming Chen, Umeshwar Dayal and Mei-chun Hsu
  IEEE Computer Society, 2001, pages 215
 */

#include <qtiprefixspan.h>
#include <iostream>

#include "FeatureClusterer.h"

using namespace std;

void QTIPrefixspan::readdatabase(istream &data) {
	_database= _factory->createDatabase(data);
}

void QTIPrefixspan::addTransaction(const sequence &seq) {
	_database.push_back( make_pair(_database.size(), seq) );
}

void QTIPrefixspan::output_pattern(const ProjDB &projected) {
#if OUTPUT_SHOW>0
	cout << "PATTERN: " << p << " [" << projected.indexes.size() <<"]" << endl;
#endif
#if OUTPUT_SHOW>1
	//transaction list
	cout << "\t{ ";
	for (auto it : projected.indexes) {
		cout << "(" << it.first <<"," << it.second << ") ";
	}
	cout << "}"<< endl;
#endif
	if(pfpatterns) {
		pfpatterns->push_back( p.clone() );
	}
}

unsigned int QTIPrefixspan::run(unsigned int _min_sup, list< pattern* > *patlist, unsigned int _max_pat) {
	pfpatterns=patlist;
	if(patlist)	patlist->clear();
	if(_min_sup) min_sup=_min_sup-1;
	max_pat=_max_pat;
	if( !min_sup ) min_sup = _database.size()/10; //defaut minsup 10%

	ProjDB projected;
	for(unsigned int i=0; i<_database.size(); i++) {
		projected.indexes.push_back( make_pair(i, -1) );
	}
	return project(projected);
}


/********************************************************************
 * Project database
 ********************************************************************/
unsigned int QTIPrefixspan::project(ProjDB &projected) {
	//cout << "project "<< endl;
	if (projected.indexes.size() < min_sup) return 0;

	//give its support to the pattern
	p.setSupport( projected.indexes.size() );

	if (max_pat != 0 && p.size() == max_pat) {
		output_pattern(projected);
		return projected.indexes.size();
	}

	//cout << "\texplore possible extensions"<< endl;

	map<unsigned int, itemcount > map_item;     // seqs: association an item to a set of sequences in which there is an item

	for( auto pos : projected.indexes ) {
		const sequence &seq=_database[ pos.first ].second;

		//evaluating the possible extensions
		unsigned int iter = (pos.second+1); //iter is the position of the last (it is safely an "unsigned int" because of -1 at the beginning and >0 after)

		for (; iter<seq.size(); iter++) {
			itemcount &itc = map_item[ seq[iter].id() ]; //itemcount of the current item
			if( itc.first_found<0 ) {
				if( itc.transactions.size()==min_sup ) {
					itc.first_found = pos.first;
					itc.last_indexes.push_back( make_pair(pos.first,iter) );
					itc.last_transaction=pos.first;
				} else {
					itc.transactions.insert( pos.first );
				}
			} else {
				if( itc.last_transaction!=pos.first ) { //this sequence has never been added !
					itc.last_transaction=pos.first;
					itc.last_indexes.push_back( make_pair(pos.first,iter) );
				}
			}
		}
	}

	//creation of a new data on which project (data copy)
	ProjDB db;
	vector< pair<unsigned int, int> > &new_indexes = db.indexes;
	bool maximal =true;

	for (auto mi : map_item) {// TODO parallelize this loop

		//cout << "\ttry extension by "<<mi.first<< endl;

		if(mi.second.first_found<0) {
			//the extension is not frequent
			continue;
		}

		for( auto pos : projected.indexes ) {
			if(pos.first >= (unsigned int)mi.second.first_found) break;

			const sequence &seq = _database[pos.first].second;
			for (unsigned int iter = pos.second+1; iter < seq.size(); iter++) {
				if( seq[iter].id() == mi.first ) {
					new_indexes.push_back( pair<unsigned int, unsigned int>(pos.first, iter) );
					break;
				}
			}
		}
		//copy the end of the indexes (previously computed
		new_indexes.insert(new_indexes.end(), mi.second.last_indexes.begin(), mi.second.last_indexes.end());


		if (new_indexes.size() < min_sup) continue;
		p.push_back( mi.first );

		//cout << "\tfrequent => specification "<<mi.first<< endl;

		//TODO: insert feature specification HERE !
		int nb=specify(db);

		db.clear();
		p.pop_back();
		if(nb>0) maximal=false;
	}

	if (print_maximal_only ) {
		if( maximal ) {
			output_pattern(projected);
		}
	} else {
		output_pattern(projected);
	}

	return projected.indexes.size();
}


/**
 * \return the number of frequent patterns that are more specified than the current one
 */
unsigned int QTIPrefixspan::specify(ProjDB &db) {
	signature sig = p.getSignature();
	int nb=0;

	vector<featureset *> tocluster;
	for(auto index : db.indexes) {
		list<featureset *> found = project(_database[index.first].second,sig);
		list<featureset *>::iterator it=found.begin();
		while(it != found.end() ) {
			if( !  ((**it)[0]) ) { //(**it)[0] is the first feature of the current featureset (if null, then we consider that there is no feature associated !)
				//if null, then we remove it from the found list
				it = found.erase(it);
			} else {
				(*it)->setSID(index.first);
				it++;
			}
		}
		tocluster.insert(tocluster.end(), found.begin(), found.end());
	}
	//cout << "\tto cluster "<< tocluster.size() << endl;
	if( tocluster.empty() ) {
		//cout << "\tno specification !" << endl;
		project(db); //recursive call without specification
		return 1;
	}

	FeatureClusterer clusterer;
	clusterer.setData( tocluster );
	//clusterer.cluster_bestk( 1, max_clusters );
	clusterer.cluster_xpam();

	const vector<int>& centres = clusterer.centroids();
	const vector<vector<int> >& clusters = clusterer.clusters();
	ProjDB sdb;
	for(unsigned int c=0; c< clusters.size(); c++) {
		//1) list all the sequences that support the pattern
		set<int> sids;
		list<const featureset *> fset;
		for(auto i : clusters[c]) {
			sids.insert( tocluster[i]->sid() );
			fset.push_back( tocluster[i] );
		}
		//2) if the set is not enough frequent: pruned
		if( sids.size()<min_sup ) continue;
		// if it is frequent:

		//3) specify the pattern from all the featuresets
		p.setPatternFeature( _factory->createPatternFeature(tocluster[centres[c]], fset) );

		//4) build a new projected database
		vector< pair<unsigned int, int> > &new_indexes = sdb.indexes;
		set<int>::iterator sit;
		for(auto index : db.indexes) {
			if( (sit=sids.find(index.first))!=sids.end() ) { //found
				new_indexes.push_back(index);
				sids.erase(sit); //remove because it will not be useful in the next searches
			}
		}

		nb += project(sdb); //recursive call
		sdb.clear();
	}

	return nb;
}

/**
 * for all possible subsequences of the signature, the algorithm generate a featureset corresponding to the
 */
list<featureset*> QTIPrefixspan::project(sequence &seq, signature &s) const {
	list<featureset*> output;
	list<featureset*> current;

	//cout << "\tproject sequence on signature"<<endl;

	if(s.empty()) return output;
	if( s.size()>seq.size() ) {
		// no output
		return output;
	}

	for(auto e : seq) {
		//cout << "test " << s[0] << "==" << e.id() << endl;
		list<featureset*>::iterator itocc = current.begin();
		list<featureset*> toadd;
		while(itocc != current.end() ) {
			if(!(*itocc)) continue;
			//cout << "test " << s[(*itocc)->dim()] << "==" << e.id() << endl;
			if( s[(*itocc)->dim()] == e.id() ) {
				//current.push_back( (*itocc)->clone() );// add a new elements in the current for possible alternative matches
				toadd.push_back( (*itocc)->clone() );// add a new elements in the current for possible alternative matches
				(*itocc)->append(&e.f());
				if( (*itocc)->dim()==s.size() ) {
					//all id have been found: a featureset is moved into the output list
					output.push_back((*itocc));
					itocc = current.erase(itocc);
				} else {
					itocc++;
				}

			} else {
				itocc++;
			}
		}
		current.insert(current.end(), toadd.begin(), toadd.end());
		if( s[0] == e.id() ) { //add a new possible matching
			featureset *fs = _factory->createFeature(e);
			if( s.size()==1 ) {
				//all id have been found: a featureset is moved into the output list
				output.push_back(fs);
			} else {
				current.push_back(fs);
			}
			//cout << "\tfound " << e.id() << endl;
		}
	}
	//remove
	for(auto occ : current) {
		delete(occ);
	}

	//cout << "\t>>out"<<endl;

	return output;
}


