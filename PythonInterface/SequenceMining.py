# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 18:31:29 2015

@author: tguyet
"""

from data import *
from prefixspan_module import *

class SubSequence(Sequence):
    """A subsequence is a sequence that have been extracted from an original 
    sequence. It has an identifier of the sequence from which it is extracted 
    from and a timestamp that "localise" the original subsequence in it..
    The SubSequence is a deepcopy of the original sequence.
    """
    def __init__(self):
        Sequence.__init__(self)
        self.sequenceid=-1
        self.begin=0.0  #!< the timestamp of the current sequence in the sequence
        self.end=0.0
        self.cl=None    #!< class attribute
        

class ExamplesGenerator:
    """
    Generate examples from a single sequence
    TODO: add a dictonary of the vocabulary ??
    TODO: get a sequence id (must be in the sequence !)
    """
    def __init__(self, id=-1, length=100, exclusionList=[], reverse=False):
        self.id = id
        self.length = length
        self.reverse = reverse
        self.exclusionList=exclusionList
    
    def windowsExtract(self, seq): #id, length, reverse=False):
        """Extract a list of examples
        Examples are SubSequences
        TODO reverse option, this option reverse the time
        TODO fill option
        """
        database=list()
        
        for it in seq.find(self.id):
            S = SubSequence()
            wbegin=it.begin-self.length
            
            wend=it.begin
            #print "windows [%d,%d]"%(wbegin, wend)
            for e in seq:
                if( e.begin>wend ):
                    break       #finished (seq assumed to be ordered)
                if e.end<wbegin:
                    continue
                if e in self.exclusionList: #exclude this kind of items
                    continue
                ee=deepcopy(e)
                ee.begin = ee.begin-wbegin
                ee.end = ee.end-wbegin
                S.add(ee , False)
            if not len(S)==0:
                S.cl=id
                S.begin= wbegin #starting time of the window
                S.end= wend     #finishing time of the window
                database.append(S)
        return database
        
    def fillTime(self, seq, fid):
        """
        This function transforms the sequences into a new sequence in which the
        intervals without events are filled with new events informing about the 
        emptyness of time !
        TODO to implement
        """
        return seq


class SequenceDatabase_iterator:
    def __init__(self, db):
        self.db = db
        self.i=0
        
    def __iter__(self):
        return self
        
    def next(self):
        if self.i>=len(self.db.db):
            raise StopIteration
        else:
            s=self.db.db[self.i]
            self.i+=1
            return s

class SequenceDatabase:
    def __init__(self):
        self.db=list()
        
    def clear(self):
        del self.db[:]
        
    def add(self, seq):
        """
        add a sequence or a list of subsequences to the database
        sequence must be objects of the SubSequence class
        """
        if isinstance(seq, list) or isinstance(seq,tuple):
            for s in seq:
                self.add(s)
        else:
            if not isinstance(seq,SubSequence):
                print("SequenceDatabase::add: only SebSequence are possible")
                return False
            self.db.append(seq)
            
    def __len__(self):
        return len(self.db)
        
    def __iter__(self):
        return SequenceDatabase_iterator(self)

class Pattern(Sequence):
    def __init__(self):
        Sequence.__init__(self)
        self.frequency=0
        self.origin=-1 #can be used to keep the sequence that has been used to extract this pattern
        
    def __cmp__(self, x):
        if len(self.seq)<len(x.seq):
            return -1
        elif len(self.seq)>len(x.seq):
            return 1
        else:
            return 0

class SequenceMiner:
    def __init__(self):
        self.db=SequenceDatabase()
        self.threshold=10
        self.patterns=[]
        self.maxlen = 0
    
    def setDB(self, db):
        self.db=db
        
    def add(self, seq):
        self.db.add(seq)
    
    def sortPatterns(self):
        self.patterns.sort(reverse=True)
        
    def mine(self, threshold=None):
        if not threshold is None:
            self.threshold=threshold
            
        #generate te database in the require format: list of list of 3-tuples (id, begin, end)
        self.database=[]
        for ss in self.db:
            seq=[]
            for e in ss:
                ee = [e.id, e.begin, e.end]
                seq.append(tuple(ee))
            self.database.append(seq)
            
        # launch mining
        pats=QTIPrefixSpan(self.database, self.threshold, self.maxlen)
        
        #create patterns object from the 
        self.patterns=[]
        for p in pats:
            pat = Pattern()
            pat.frequency = p[1]
            for t in p[0]:
                e = Event(t[0],t[1],t[2])
                pat.add(e)
            self.patterns.append(pat)
        

if __name__ == '__main__':
    #########################   
    ### Some simple tests ###
    #########################
        
    s=Sequence()
    
    s.add( Event(1,24.0,35) )
    s.add( Event(2,34.0,45.0) )
    s.add( Event(3,56.0,67.0) )
    s.add( Event(2,12.0,20.0) )
    
    s.sort()
    
    print("--- test database generation ---")
    gen = ExamplesGenerator(2,10)
    print "dataset:"+str(s)
    ss=gen.windowsExtract(s)
    print ss
    
    print("--- test sequence database ---")
    db = SequenceDatabase()
    db.add( ss )
    for s in db:
        print s
        
        
    print("--- test sequence miner ---")
    miner = SequenceMiner()
    miner.add( ss )
    miner.mine(1)