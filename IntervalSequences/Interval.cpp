/*
 * Interval.cpp
 *
 *  Created on: Aug 4, 2015
 *      Author: tguyet
 */

#include "Interval.h"
#include <iomanip>
#include <cmath>

/**
 * Operateur d'inclusion stricte
 */
bool operator<(Interval const &I1, Interval const &I2)
{
	return I1<I2 && !( I1==I2 );
}

bool operator<=(Interval const &I1, Interval const &I2)
{
	return I1.u()<=I2.u() && I1.l()>=I2.l();
}

bool operator==(Interval const &I1, Interval const &I2)
{
	return I1.u()==I2.u() && I1.l()==I2.l();
}

bool operator!=(Interval const &I1, Interval const &I2)
{
	return !(I1==I2);
}


void Interval::printcsv(std::ostream& out) const {
	out << l() << ";" << u();
}

std::ostream& operator<<(std::ostream& out, const Interval& I) // output
{
    out << std::setw(3) << std::setprecision(2) << "[" << I.l() << ", " << I.u() << "]";
    return out;
}
