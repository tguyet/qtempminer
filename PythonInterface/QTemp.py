# -*- coding: utf-8 -*-
"""
author: T. Guyet
date: 09/2015
"""

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
from PyQt4.QtGui import QApplication, QCursor

from data import Sequence, Dataset, loadDataset, loadCsvDataset
from SequenceMining import *
from Recognizer import PatternRecognizer

from patternviewer import PatternViewer

from functools import partial

import numpy as np
import os
from scipy.stats import norm

"""
\param ql number of symbols
\param sigma standard deviation of the distribution (centered)
\return a vector of breakpoints
"""
def quantification_levels(ql, sigma =1):
    bp=norm.ppf((np.arange(ql, dtype=float)+1)/(ql+1), scale=sigma)
    return bp

def sax(ts, l, ql):
    #spliting ts
    pos=range(l, len(ts), l)
    ta=np.split(ts, pos)
    t=np.zeros(len(ta), dtype=np.float)
    for i in range(len(ta)):
        t[i]=np.mean(ta[i])
    #quantization
    bp = quantification_levels(ql)
    sts = np.zeros(len(t), dtype=np.uint)
    for i in range(ql-1):
        pos = list(set(np.where(t<bp[i+1])[0]) & set(np.where(t>bp[i])[0]))
        sts[pos]=i
    
    sts[ np.where(t>bp[ql-1])[0] ]=ql-1
    return sts



pg.mkQApp()

## Define main window class from template
path = os.path.dirname(os.path.abspath(__file__))
uiFile = os.path.join(path, 'main.ui')
WindowTemplate, TemplateBaseClass = pg.Qt.loadUiType(uiFile)


class MainWindow(TemplateBaseClass):  
    def __init__(self):
        TemplateBaseClass.__init__(self)
        self.setWindowTitle('QTempIntMiner interface')
        
        self.ts=None
        self.s = Sequence()
        self.dataset=Dataset()
        self.newdb=False
        self.miner = SequenceMiner()
        
        self.default_high=60
        
        # Create the main window
        self.ui = WindowTemplate()
        self.ui.setupUi(self)
        self.ui.plot.setMouseEnabled(True, False)
        
        #--- connections ---
        #self.ui.loadBtn.clicked.connect(self.plot)
        self.ui.generateBtn.clicked.connect(self.generate)
        self.ui.mineBtn.clicked.connect(self.mine)
        self.ui.buttonClearAnnotation.clicked.connect(self.clearAnnotation)
        self.ui.selectSequenceBox.activated[str].connect(self.sequenceChanged)
        self.ui.replotBtn.clicked.connect(self.replot)
        #self.ui.allSequencesCkb.stateChanged.connect(self.changeItemsCombo)
        
        #--- menu ---
        self.ui.actionOpen.triggered.connect(self.open)
        self.ui.actionQuit.triggered.connect(QtGui.qApp.quit)
        self.ui.actionAbout.triggered.connect(self.about)
        self.ui.actionLoadTS.triggered.connect(self.loadTS)
        
        self.ui.actionQuick_launch.triggered.connect(self.qlaunch)
        self.ui.actionQuick_launch_ECG.triggered.connect(self.qlaunch_ecg)
        self.ui.actionQuick_launch_Muscles.triggered.connect(self.qlaunch_muscles)
        #---- end menu ---        
        
        
        #--- example list preparation ---
        self.databaseLayout = QtGui.QVBoxLayout()
        self.databaseLayout.addWidget( QtGui.QLabel("Sequence database") )
        self.ui.databaseArea.widget().setLayout(self.databaseLayout)
        self.databaseLayout.setSizeConstraint(self.databaseLayout.SetFixedSize)
        
        #--- pattern list preparation ---
        self.patternsAreaLayout = QtGui.QVBoxLayout()
        self.patternsAreaLayout.addWidget( QtGui.QLabel("Patterns") )
        self.ui.patternsArea.widget().setLayout(self.patternsAreaLayout)
        self.patternsAreaLayout.setSizeConstraint(self.patternsAreaLayout.SetFixedSize)
        
        #--- pattern collection prepratation ---    
        self.patternsCollectionAreaLayout = QtGui.QHBoxLayout()
        self.patternsCollectionAreaLayout.addWidget( QtGui.QLabel("Patterns") )
        self.ui.patternsCollectionArea.widget().setLayout(self.patternsCollectionAreaLayout)
        self.patternsCollectionAreaLayout.setSizeConstraint( self.patternsCollectionAreaLayout.SetFixedSize)    
        
        #--- recognition variables
        self.patternsCollection = list() #list of the interesting patterns that can be use to detect events
        self.annotation = None        
        
        self.show()
        self.plot()
    
    def about(self):
        QtGui.QMessageBox.about(self, "About",
"""QTempIntMiner Interface
Copyright 2015 Thomas Guyet, Inria - Agrocampus-Ouest/IRISA

This program is an interface to the QTempIntMiner algorithm.
"""
)

    def qlaunch(self):
        self.dataset=loadCsvDataset("/home/tguyet/Progs/QTempIntMinerCpp/PythonInterface/dataMongo.csv", 200)
        #clear plot
        self.ui.plot.clear()
        
        #fill the sequence combobox
        self.ui.selectSequenceBox.clear()
        for i in range(len(self.dataset)):
            self.ui.selectSequenceBox.addItem( str(i) )
            
        #fill the items combobox
        for i in self.dataset.itemsList():
            self.ui.itemsCombo.addItem( str(i) )
            
        #selection of the sequence
        self.sequenceChanged(0)
        #"""""""""""""""
        generator = ExamplesGenerator(2,10000)
        self.miner.db = generator.windowsExtract( self.s )
        #update interface
        self.newdb=True
        self.plot_database()
        
        #"""""""""""""""
        self.miner.db = self.miner.db[:200]
        self.miner.mine(6)
        #update interface
        self.plot_patterns()
        
        #================
        self.ui.recognition_threshold.setValue(15)
        
    def qlaunch_ecg(self):
        self.dataset=loadCsvDataset("/home/tguyet/Progs/QTempIntMinerCpp/PythonInterface/ecg_annots.csv")
        #clear plot
        self.ui.plot.clear()
        
        #fill the sequence combobox
        self.ui.selectSequenceBox.clear()
        for i in range(len(self.dataset)):
            self.ui.selectSequenceBox.addItem( str(i) )
            
        #fill the items combobox
        for i in self.dataset.itemsList():
            self.ui.itemsCombo.addItem( str(i) )
            
        #selection of the sequence
        self.sequenceChanged(1)
        #"""""""""""""""
        generator = ExamplesGenerator(1,250)
        self.miner.db = generator.windowsExtract( self.s )
        #update interface
        self.newdb=True
        self.plot_database()
        
        #"""""""""""""""
        self.miner.mine(6)
        #update interface
        self.plot_patterns()
        
        #================
        self.ui.recognition_threshold.setValue(15)
        
    def qlaunch_muscles(self):
        self.dataset=loadCsvDataset("/home/tguyet/Progs/QTempIntMinerCpp/PythonInterface/muscle.csv")
        #clear plot
        self.ui.plot.clear()
        
        #fill the sequence combobox
        self.ui.selectSequenceBox.clear()
        for i in range(len(self.dataset)):
            self.ui.selectSequenceBox.addItem( str(i) )
            
        #fill the items combobox
        for i in self.dataset.itemsList():
            self.ui.itemsCombo.addItem( str(i) )
        
        print "sequence loaded"
        #selection of the sequence
        self.sequenceChanged(0)
        #"""""""""""""""
        generator = ExamplesGenerator(21,100)
        self.miner.db = generator.windowsExtract( self.s )
        #update interface
        self.newdb=True
        self.plot_database()
        print "db generated"
        #"""""""""""""""
        self.ui.thresholdSpinBox.setValue(15)
        self.miner.mine(15)
        #update interface
        self.plot_patterns()
        print "pattern extracted"
        
        #================
        self.ui.recognition_threshold.setValue(10)
        
    def loadTS(self):
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '')
        if len(fname)!=0:
            filename, file_extension = os.path.splitext(str(fname))
            self.ts=Timeserie(fname)
            self.ts.center()
                
    def open(self):
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '')
        if len(fname)!=0:
            filename, file_extension = os.path.splitext(str(fname))
            if file_extension==".pkl":
                QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
                self.dataset=loadDataset(fname)
                QApplication.restoreOverrideCursor()
                QtGui.QMessageBox.information(self, 
                 self.trUtf8("Open file"), 
                 self.trUtf8("{0} sequences have been successfully loaded.".format(len(self.dataset))))
            elif file_extension==".csv":
                QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
                self.dataset=loadCsvDataset(fname)
                QApplication.restoreOverrideCursor()
                QtGui.QMessageBox.information(self, 
                 self.trUtf8("Open file"), 
                 self.trUtf8("{0} sequences have been successfully loaded.".format(len(self.dataset))))
            else:
                QtGui.QMessageBox.warning(self, 
                 self.trUtf8("Open file"), 
                 self.trUtf8("Warning! Unknown file extension."))
                return
            
            #update interface
            QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
            #clear plot
            self.ui.plot.clear()
            
            #fill the sequence combobox
            self.ui.selectSequenceBox.clear()
            for i in range(len(self.dataset)):
                self.ui.selectSequenceBox.addItem( str(i) )
                
            #fill the items combobox
            self.ui.itemsCombo.clear()
            self.ui.itemExclusionList.clear()
            for i in self.dataset.itemsList():
                self.ui.itemsCombo.addItem( str(i) )
                
                item = QtGui.QListWidgetItem(self.ui.itemExclusionList)
                item.setText(str(i))
                item.setFlags(QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsUserCheckable)
                item.setCheckState(QtCore.Qt.Unchecked)
            
            if len(self.dataset)>0:
                self.sequenceChanged(0)
            QApplication.restoreOverrideCursor()
    
    def sequenceChanged(self, text):
        self.s = self.dataset[int(text)]
        self.ui.plot.clear()
        self.plot()

    """
    def changeItemsCombo(self, state):
        if state == QtCore.Qt.Checked:
            self.itemsCombo.setEnabled(False)
        else:
            self.itemsCombo.setEnabled(True)
    """     
    def generate(self):
        """The function generate a database of examples.
        """
        id = int(self.ui.itemsCombo.currentText())
        length = self.ui.lengthSpinBox.value()

        exclusionList=[]
        #for item in self.ui.itemExclusionList:
        for i in range(self.ui.itemExclusionList.count()):
            item = self.ui.itemExclusionList.item(i)
            if item.checkState() == QtCore.Qt.Checked:
                exclusionList.append( int(item.text()) )
        
        generator = ExamplesGenerator(id, length, exclusionList)
        
        QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
        if self.ui.allSequencesCkb.checkState()==QtCore.Qt.Checked:
            #extract examples from a dataset of sequences
            self.miner.db.clear()
            for s in self.dataset:
                ldb=generator.windowsExtract( s )
                for ls in ldb:
                    self.miner.db.add(ls)
        else:
            # extract the windows just for the current sequence
            self.miner.db = generator.windowsExtract( self.s )
        
        self.newdb=True
        
        #update interface
        self.plot_database()
        self.ui.generationInfoLabel.setText("#generated examples: {0}".format(len(self.miner.db)))
        QApplication.restoreOverrideCursor()
        
        
    def plot_database(self):
        for i in reversed(range(self.databaseLayout.count())): 
            self.databaseLayout.itemAt(i).widget().setParent(None)
            
        i=0
        for seq in self.miner.db:
            #create a new ploting widget
            widget = pg.PlotWidget()
            widget.setFixedHeight(self.default_high)
            widget.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
            #widget.setMaximumSize(300, 50)
            widget.setMouseEnabled(False, False)# avoid possible interactions
            self.databaseLayout.addWidget( widget )

            #plot the sequence in this widget
            seq.autoConfig()
            seq.plot( widget.getPlotItem() )
            i+=1
            if i==100:
                """
                QtGui.QMessageBox.information(self, 
                 self.trUtf8("DB Generation"), 
                 self.trUtf8("{0} sequences have been successfully generated but only first 100th are shown.".format(len(self.miner.db))))
                """
                break
    
    def replot(self):
        self.ui.plot.clear()
        self.plot()
        
    def plot(self):
        self.s.autoConfig()
        self.s.plot(self.ui.plot)
        
        if self.ts!=None:
            self.ui.plot.plot( self.ts.data )
        
    def getPatternDetails(self, i):
        #print "pattern ", i , ":", self.miner.patterns[int(i)]
        viewer = PatternViewer(self.miner.patterns[int(i)])
        viewer.exec_()

    def addPattern(self, i):
        #print "add pattern ", i , ":", self.miner.patterns[int(i)]
        self.patternsCollection.append( self.miner.patterns[int(i)] )
        self.updatePatternsCollection()

    def updatePatternsCollection(self):
        """Function that update the interface by fill up the patternCollectionArea
        """
        QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
        #clear
        for i in reversed(range(self.patternsCollectionAreaLayout.count())): 
            if self.patternsCollectionAreaLayout.itemAt(i).widget():
                self.patternsCollectionAreaLayout.itemAt(i).widget().setParent(None)
            else:
                self.patternsCollectionAreaLayout.itemAt(i).layout().setParent(None)
        i=0
        for pat in self.patternsCollection:
            vLayout = QtGui.QVBoxLayout()
            #create a new ploting widget
            widget = pg.PlotWidget()
            widget.setFixedWidth(200)
            widget.setFixedHeight(100)
            widget.setSizePolicy( QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed )
            widget.setMouseEnabled(False, False)
            
            button = QtGui.QPushButton("rechercher")
            button.clicked.connect( partial(self.recognize, str(i)) )
            vLayout.addWidget( widget )
            vLayout.addWidget( button )
            
            self.patternsCollectionAreaLayout.addLayout( vLayout )
            
            #plot the sequence in this widget
            pat.autoConfig()
            pat.plot( widget.getPlotItem() )
            i+=1
        QApplication.restoreOverrideCursor()
    
    def recognize(self, i):
        """Function that triggers the pattern recognition in the sequence
        """
        i=int(i)
        #get parameters values
        th=self.ui.recognition_threshold.value()
        
        #define the recognizer
        recognizer = PatternRecognizer(self.patternsCollection[i], th)
        
        #apply recognition
        QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
        found=recognizer.find_signatures(self.s)
        print found
        
        #draw the research as a new sequence
        self.annotation = Sequence()
        maxval = max(self.dataset.itemsList())
        for instance in found:            
            e = Event(maxval+2+i, self.s[instance[0]].begin, self.s[instance[-1]].end)
            self.annotation.add(e)
        
        self.annotation.autoConfig()
        self.annotation.plot(self.ui.plot)
        QApplication.restoreOverrideCursor()
    
    def clearAnnotation(self):
        if not self.annotation is None:
            self.annotation = None
            self.ui.plot.clear()
            self.plot()
    
    def plot_patterns(self):
        QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
        for i in reversed(range(self.patternsAreaLayout.count())): 
            if self.patternsAreaLayout.itemAt(i).widget():
                self.patternsAreaLayout.itemAt(i).widget().setParent(None)
            else:
                self.patternsAreaLayout.itemAt(i).layout().setParent(None)
            
        i=0
        for seq in self.miner.patterns:
            #create a layout
            hLayout = QtGui.QHBoxLayout()
            vLayout = QtGui.QVBoxLayout()
            #create a new ploting widget
            widget = pg.PlotWidget()
            widget.setFixedHeight(self.default_high)
            widget.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
            widget.setMouseEnabled(True, False)
            
            button = QtGui.QPushButton("details")
            button.clicked.connect( partial(self.getPatternDetails, str(i)) )
            
            buttonadd = QtGui.QPushButton("add")
            buttonadd.clicked.connect( partial(self.addPattern, str(i)) )
            
            vLayout.addWidget( button )
            vLayout.addWidget( buttonadd )
            hLayout.addLayout( vLayout )
            hLayout.addWidget( widget )
            self.patternsAreaLayout.addLayout( hLayout )
            #plot the sequence in this widget
            seq.autoConfig()
            seq.plot( widget.getPlotItem() )
            i+=1
            if i==200:
                QtGui.QMessageBox.information(self, 
                 self.trUtf8("Patterns drawing"), 
                 self.trUtf8("{0} patterns have been successfully generated but only first 100th are shown.".format(len(self.miner.patterns))))
                break
        QApplication.restoreOverrideCursor()
                
    def mine(self):
        if len(self.miner.db)==0:
            QtGui.QMessageBox.information(self, 
             self.trUtf8("Mine patterns"), 
             self.trUtf8("No sequence to mine."))
            return
        th = int(self.ui.thresholdSpinBox.value() * len(self.miner.db)/100)
        if self.newdb==False and self.miner.threshold==th:
            QtGui.QMessageBox.information(self, 
             self.trUtf8("Mine patterns"), 
             self.trUtf8("Patterns will not change (same database, same threshold)."))
            return
        
        QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
        #launch mining
        self.miner.mine( th )
        self.newdb=False

        #update interface
        self.miner.sortPatterns()
        
        # add the if of the current as origin of the pattern
        # HACK not safe at all !!
        for p in self.miner.patterns:
            p.origin = int( self.ui.selectSequenceBox.currentText())
            
        self.plot_patterns()
        QApplication.restoreOverrideCursor()
        
win = MainWindow()

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
