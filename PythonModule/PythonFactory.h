/*
 * PythonFactory.h
 *
 *  Created on: Aug 7, 2015
 *      Author: tguyet
 */

#ifndef PYTHONMODULE_PYTHONFACTORY_H_
#define PYTHONMODULE_PYTHONFACTORY_H_

#include "IntervalSequences/IntervalEvent.h"
#include "QTI/pattern.h"
#if defined(_WIN32) || defined(WIN32)
#include "Python.h"
#else
#include "python2.7/Python.h"
#endif


/**
 * a factory for
 */
class PythonFactory: public IntervalSequenceFactory {
public:
	PythonFactory();
	virtual ~PythonFactory();

	sequence createSequence(PyObject *item);
	vector<sequence> createDatabase(PyObject *list);

	PyObject* createPattern(const pattern& pat);
	PyObject* createPatternsList(const list<pattern*> &pat);

protected:
	event createEvent(PyObject *item);
};

#endif /* PYTHONMODULE_PYTHONFACTORY_H_ */
