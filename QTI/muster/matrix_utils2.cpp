/*
 * matrix_utils2.cpp
 *
 *  Created on: 2 oct. 2015
 *      Author: tguyet
 */

#include "matrix_utils2.h"

namespace cluster {

double sum( const dissimilarity_matrix &matrix ) {
	double total=0;
	for (size_t i = 0; i < matrix.size1(); i++) {
		for (size_t j = 0; j < i; j++) {
			total += matrix(i,j);
		}
	}
	return total*2;
}

};

