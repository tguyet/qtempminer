# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 23:04:40 2015

@author: tguyet
"""


import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
import os

Ui_PatternViewer = pg.Qt.loadUiType(os.path.join( os.path.dirname( __file__ ), 'patternviewer.ui' ))[0]


class PatternViewer(QtGui.QDialog):
    def __init__(self, p):
        QtGui.QDialog.__init__(self)
        self.ui = Ui_PatternViewer()
        self.ui.setupUi(self)
        self.ui.plotWidget.setMouseEnabled(True, False)        
                
        self.pattern = p

        #update interface
        self.ui.labelFrequency.setText("Frequency: {0}".format(self.pattern.frequency))
        
        self.pattern.autoConfig()        
        self.pattern.plot( self.ui.plotWidget )