#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx

import sys
sys.path.insert(0, "/udd/tguyet/workspace/QTempInt/lib")
from prefixspan_module import *

#load database of sequence from a file
minval=1e100
maxval=-1e100
database=[]
f = open('../tests/example.txt', 'r')
for lines in f:
	events=lines.rstrip('\n;').split(";")
	sequence=[]
	for e in events:
		vals = [float(ff) for ff in e.split(" ")]
		vals[2] = vals[2]+vals[1] #the last value of the file is a duration, transformed into a finishing date
		if vals[0]<minval: minval=vals[0]
		if vals[0]>maxval: maxval=vals[0]
		sequence.append( tuple(vals) )
	database.append(sequence)
f.close()

#print database

#generate an appropriate colormap for graphics
jet = plt.get_cmap('jet')
cNorm  = colors.Normalize(vmin=minval, vmax=maxval)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)


#generate a graphic for the database
fig, ax = plt.subplots()
i=0
for sequence in database:
	intervals=[]
	symbols=[]
	for t in sequence:
		intervals.append( tuple([t[j] for j in [1,2]]) )
		colorVal = scalarMap.to_rgba( t[0] )
		symbols.append( colorVal )
	ax.broken_barh(intervals , (i, .8), facecolors=symbols)
	i+=1

ax.set_xlabel('seconds since start')
ax.grid(True)
plt.draw()


#mine the patterns
pat=QTIPrefixSpan(database, 15)


#show the extraced patterns
fig, ax = plt.subplots()
i=0
for p in pat:
	intervals=[]
	symbols=[]
	for t in p[0]:
		intervals.append( tuple([t[j] for j in [1,2]]) )
		colorVal = scalarMap.to_rgba( t[0] )
		symbols.append( colorVal )
	ax.broken_barh(intervals , (i, .8), facecolors=symbols, alpha=.5)
	i+=1

ax.set_xlabel('seconds since start')
ax.grid(True)
plt.draw()

#extract the 'largest pattern' (useful for this database!!)
maxpat=([],0)
for p in pat:
	try:
		if len(p[0])>len(maxpat[0]):
			maxpat=p
		elif len(p[0])==len(maxpat[0]):
			if p[1]>maxpat[1]: #same length, better support
				maxpat=p
	except IndexError:
		pass

print "=============="
print maxpat

fig, ax = plt.subplots()
intervals=[]
symbols=[]
for t in maxpat[0]:
	intervals=[tuple([t[j] for j in [1,2]])]
	colorVal = scalarMap.to_rgba( t[0] )
	symbols= colorVal
	ax.broken_barh(intervals , (t[0], .8), facecolors=symbols, alpha=.5)
ax.set_xlabel('seconds since start')
ax.grid(True)
plt.show()


	

