//////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, Lawrence Livermore National Security, LLC.  
// Produced at the Lawrence Livermore National Laboratory  
// LLNL-CODE-433662
// All rights reserved.  
//
// This file is part of Muster. For details, see http://github.com/tgamblin/muster. 
// Please also read the LICENSE file for further information.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the disclaimer below.
//  * Redistributions in binary form must reproduce the above copyright notice, this list of
//    conditions and the disclaimer (as noted below) in the documentation and/or other materials
//    provided with the distribution.
//  * Neither the name of the LLNS/LLNL nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// LAWRENCE LIVERMORE NATIONAL SECURITY, LLC, THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//////////////////////////////////////////////////////////////////////////////////////////////////

///
/// @file kmedoids.h
/// @author Todd Gamblin tgamblin@llnl.gov
/// @brief Implementations of the classic clustering algorithms PAM and CLARA, from 
/// <i>Finding Groups in Data</i>, by Kaufman and Rousseeuw.
///
#ifndef K_MEDOIDS_H
#define K_MEDOIDS_H

#include <vector>
#include <set>
#include <iostream>
#include <stdexcept>
#include <cfloat>

#include <boost/random.hpp>

#include "random.h"
#include "dissimilarity.h"
#include "partition.h"
#include "bic.h"

namespace cluster {

  /// 
  /// Implementations of the classic clustering algorithms PAM, from
  /// <i>Finding Groups in Data</i>, by Kaufman and Rousseeuw.
  /// 
  class kmedoids : public partition {
  public:
    ///
    /// Constructor.  Can optionally specify number of objects to be clustered.
    /// and this will start out with all of them in one cluster.
    /// 
    /// The random number generator associated with this kmedoids object is seeded
    /// with the time in microseconds since the epoch.
    /// 
    kmedoids(size_t num_objects = 0);
    ~kmedoids();

    /// Get the average dissimilarity of objects w/their medoids for the last run.
    double average_dissimilarity() const;

    /// Set random seed.
    void set_seed(unsigned long seed);
    
    /// Set whether medoids will be sorted by object id after clustering is complete.
    /// Defaults to true.
    void set_sort_medoids(bool sort_medoids);

    /// Set tolerance for convergence.  This is relative error, not absolute error.  It will be
    /// multiplied by the mean distance before it is used to test convergence.
    /// Defaults to 1e-15; may need to be higher if there exist clusterings with very similar quality.
    void set_epsilon(double epsilon);

    /// 
    /// Classic K-Medoids clustering, using the Partitioning-Around-Medoids (PAM)
    /// algorithm as described in Kaufman and Rousseeuw. 
    ///
    /// @param distance         dissimilarity matrix for all objects to cluster
    /// @param k                number of clusters to produce
    /// @param initial_medoids  Optionally supply k initial object ids to be used as initial medoids.
    ///
    /// @see \link build_dissimilarity_matrix()\endlink, a function to automatically
    ///      construct a dissimilarity matrix given a vector of objects and a distance function.
    /// 
    void pam(const dissimilarity_matrix& distance, size_t k, const object_id *initial_medoids = NULL);

    ///
    /// Classic K-Medoids clustering, using the Partitioning-Around-Medoids (PAM)
    /// algorithm as described in Kaufman and Rousseeuw. Runs PAM from 1 to max_k and selects
    /// the best k using the bayesian information criterion.  Sets this partition to the best
    /// partition found using PAM from 1 to k.
    /// 
    /// Based on X-Means, see Pelleg & Moore, 2000.
    /// 
    /// @param distance         dissimilarity matrix for all objects to cluster
    /// @param max_k            Upper limit on number of clusters to find.
    /// @param dimensionality   Number of dimensions in clustered data, for BIC.
    ///
    /// @return the best BIC value found (the bic value of the final partitioning).
    ///
    /// @see \link build_dissimilarity_matrix()\endlink, a function to automatically
    ///      construct a dissimilarity matrix given a vector of objects and a distance function.
    ///
    double xpam(const dissimilarity_matrix& distance, size_t max_k, size_t dimensionality);

    void set_init_size(size_t sz) { init_size = sz; }
    void set_max_reps(size_t r) { max_reps = r; }

    /// Set callback function for XPAM and XCLARA.  default is none.
    void set_xcallback(void (*)(const partition& part, double bic));

protected:
    typedef boost::mt19937 random_type;                /// Type for RNG used in this algorithm
    random_type random;                                /// Randomness source for this algorithm
    
    /// Adaptor for STL algorithms.
    typedef boost::random_number_generator<random_type, unsigned long> rng_type;
    rng_type rng;

    std::vector<medoid_id> sec_nearest;      /// Index of second closest medoids.  Used by PAM.
    double total_dissimilarity;              /// Total dissimilarity bt/w objects and their medoid
    bool sort_medoids;                       /// Whether medoids should be canonically sorted by object id.
    double epsilon;                          /// Normalized sensitivity for convergence
    size_t init_size;                        /// initial sample size (before 2*k)
    size_t max_reps;                         /// initial sample size (before 2*k)


    /// Callback for each iteration of xpam.  is called with the current clustering and its BIC score.
    void (*xcallback)(const partition& part, double bic);

    /// KR BUILD algorithm for assigning initial medoids to a partition.
    void init_medoids(size_t k, const dissimilarity_matrix& distance);

    /// Total cost of swapping object h with medoid i.
    /// Sums costs of this exchagne for all objects j.
    double cost(medoid_id i, object_id h, const dissimilarity_matrix& distance) const;


    /// Assign each object to the cluster with the closest medoid.
    ///
    /// @return Total dissimilarity of objects w/their medoids.
    /// 
    /// @param distance a callable object that computes distances between indices, as a distance 
    ///                 matrix would.  Algorithms are free to use real distance matrices (as in PAM) 
    ///                 or to compute lazily (as in CLARA medoid assignment).
    template <class DM>
    double assign_objects_to_clusters(DM distance) {
      if (sec_nearest.size() != cluster_ids.size()) {
        sec_nearest.resize(cluster_ids.size());
      }
      
      // go through and assign each object to nearest medoid, keeping track of total dissimilarity.
      double total_dissimilarity = 0;
      for (object_id i=0; i < cluster_ids.size(); i++) {
        double    d1, d2;  // smallest, second smallest distance to medoid, respectively
        medoid_id m1, m2;  // index of medoids with distances d1, d2 from object i, respectively

        d1 = d2 = DBL_MAX;
        m1 = m2 = medoid_ids.size();
        for (medoid_id m=0; m < medoid_ids.size(); m++) {
          double d = distance(i, medoid_ids[m]);
          if (d < d1 || medoid_ids[m] == i) {  // prefer the medoid in case of ties.
            d2 = d1;  m2 = m1;
            d1 = d;   m1 = m;
          } else if (d < d2) {
            d2 = d;   m2 = m;
          }
        }

        cluster_ids[i] = m1;
        sec_nearest[i] = m2;
        total_dissimilarity += d1;
      }

      return total_dissimilarity;
    }
  };


} // namespace cluster

#endif //K_MEDOIDS_H
