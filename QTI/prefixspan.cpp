/**
 PrefiSpan --- An Implementation of Prefix-projected Sequential Pattern mining

 Author: Thomas Guyet <thomas.guyet@agrocampus-ouest.fr>,
	     adapted from Yasuo Tabei <tabei@cb.k.u-tokyo.ac.jp>

 Date (last update): 03/2015

 License: GPL2 (Gnu General Public License Version 2)

 Reference:
  PrefixSpan: Mining Sequential Patterns Efficiently by Prefix-Projected Pattern Growth
  Jian Pei, Jiawei Han, Behzad Mortazavi-asl, Helen Pinto, Qiming Chen, Umeshwar Dayal and Mei-chun Hsu
  IEEE Computer Society, 2001, pages 215
 */

#include "prefixspan.h"
#include <iostream>

using namespace std;


void Prefixspan::output_pattern(const ProjDB &projected) {
#if OUTPUT_SHOW>0
	cout << p << endl;
#endif
#if OUTPUT_SHOW>1
	//transaction list
	cout << endl << "( ";
	for (vector<Transaction>::const_iterator it = projected._database.begin(); it != projected._database.end(); it++) {
		cout << it->first << " ";
	}
	cout << ") : " << projected._database.size() << endl;
#endif
	if(pfpatterns) {
		pfpatterns->push_back( pair<pattern, unsigned int >(p, projected.indexes.size()) );
	}
}

unsigned int Prefixspan::run() {
	ProjDB projected;
	for(unsigned int i=0; i<_database.size(); i++) {
		projected.indexes.push_back( make_pair(i, -1) );
	}
	return project(projected);
}


/********************************************************************
 * Project database
 ********************************************************************/
unsigned int Prefixspan::project(ProjDB &projected) {
	if (projected.indexes.size() < min_sup) return 0;

	if (max_pat != 0 && p.size() == max_pat) {
		output_pattern(projected);
		return projected.indexes.size();
	}

	map<unsigned int, itemcount > map_item;     // seqs: association an item to a set of sequences in which there is an item

	for( auto pos : projected.indexes ) {
		const sequence &seq=_database[ pos.first ].second;

		//evaluating the possible extensions
		unsigned int iter = (pos.second+1); //iter is the position of the last (it is safely an "unsigned int" because of -1 at the beginning and >0 after)

		for (; iter<seq.size(); iter++) {
			itemcount &itc = map_item[ iter ]; //itemcount of the current item
			if( itc.first_found<0 ) {
				if( itc.transactions.size()==min_sup ) {
					itc.first_found = pos.first;
					itc.last_indexes.push_back( make_pair(pos.first,iter) );
					itc.last_transaction=pos.first;
				} else {
					itc.transactions.insert( pos.first );
				}
			} else {
				if( itc.last_transaction!=pos.first ) { //this sequence has never been added !
					itc.last_transaction=pos.first;
					itc.last_indexes.push_back( make_pair(pos.first,iter) );
				}
			}

		}
	}


	//creation of a new data on which project (data copy)
	ProjDB db;
	vector< pair<unsigned int, int> > &new_indexes = db.indexes;
	bool maximal =true;

	for (auto mi : map_item) {// TODO parallelize this loop
		if(mi.second.first_found<0) {
			//the extension is not frequent
			continue;
		}

		for( auto pos : projected.indexes ) {
			if(pos.first >= mi.second.first_found) break;

			const sequence &seq = _database[pos.first].second;
			for (unsigned int iter = pos.second+1; iter < seq.size(); iter++) {
				if( seq[iter].id() == mi.first ) {
					new_indexes.push_back( pair<unsigned int, unsigned int>(pos.first, iter) );
					break;
				}
			}
		}
		//copy the end of the indexes
		new_indexes.insert(new_indexes.end(), mi.second.last_indexes.begin(), mi.second.last_indexes.end());

		//TODO: insert feature specification HERE !


		p.push_back( mi.first );
		int nb = project(db);//recursive call
		p.pop_back();
		db.clear();
		if(nb>0) maximal=false;
	}


	if (print_maximal_only ) {
		if( maximal ) {
			output_pattern(projected);
		}
	} else {
		output_pattern(projected);
	}

	return projected.indexes.size();
}

