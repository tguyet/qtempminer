# -*- coding: utf-8 -*-
"""
Created on Sat Oct 10 17:26:52 2015

@author: tguyet
"""

from data import Sequence, Event
from SequenceMining import Pattern
from copy import deepcopy


class PatternRecognizer:
    def __init__(self, pattern, threshold=10):
        self.pattern = pattern
        self.th = threshold*threshold
        
        #compute multidimensional intervals
        self.p_int=list()
        for e in self.pattern:
            self.p_int.append( (e.begin, e.end) )
        
    
    def find_signatures(self, seq):
        #first, we extract signatures
        complete_instances=list()
        instances = list() # list of partial instances, where an instance is a list of positions in the sequence
        pos = 0
        pattern_duration= self.pattern[-1].end - self.pattern[0].begin
        while pos != len(seq):
            #print("test {0}".format(pos))
            toadd=list()
            toremove=list()
            i=0
            for instance in instances:
                if len(instance)==len(self.pattern):
                    toremove.append(i)
                    i+=1
                    continue
                if seq[instance[0]].begin+self.th+pattern_duration < seq[pos].begin:
                    toremove.append(i)
                    continue
                i+=1
                if seq[pos].id == self.pattern[ len(instance) ].id:
                    toadd.append( deepcopy(instance) )
                    instance.append(pos)
                if len(instance)==len(self.pattern):
                    #print("\t discover new complete instance")
                    if self.__check_temporal_constraints(seq, instance):
                        complete_instances.append( deepcopy(instance))
                        
            toremove.sort(reverse=True)
            for i in toremove:
                del instances[i]
            
            for instance in toadd:
                instances.append(instance)
            if seq[pos].id== self.pattern[0].id:
                #print("\tadd new")
                if len(self.pattern)==1:
                    if self.__check_temporal_constraints(seq, instance):
                        complete_instances.append( [pos] )
                else:
                    #create a new instance
                    instances.append( [pos] )
            pos+=1
        return complete_instances
        
        
    def __check_temporal_constraints(self, seq, instance):
        interval=list()
        for pos in instance:
            interval.append( (seq[pos].begin, seq[pos].end) )
        
        #compute euclidian distance
        d=0
        decay = interval[0][0]-self.p_int[0][0]
        #print decay
        for i in range(len(self.pattern)):
            r=self.p_int[i][0]-(interval[i][0]-decay)
            s=self.p_int[i][1]-(interval[i][1]-decay)
            d+=r*r+s*s
        
        #print d
        if d <= self.th:
            return True
        else:
            return False

if __name__ == '__main__':
    #########################   
    ### Some simple tests ###
    #########################
        
    s=Sequence()
    s.add( Event(2,12.0,20.0) )
    s.add( Event(1,24.0,34) )
    s.add( Event(2,35.0,40.0) )
    s.add( Event(1,54.0,64) )
    s.add( Event(3,56.0,67.0) )
    s.add( Event(3,58.0,67.0) )
    s.add( Event(2,64.0,71.0) )
    s.sort()
    
    s2=Sequence()
    s2.add( Event(1,24.0,34) )
    s2.add( Event(3,26.0,40.0) )
    s2.add( Event(2,32.0,35.0) )
    s2.add( Event(2,35.0,40.0) )
    s2.sort()
    
    p = Pattern()
    p.add( Event(1, 0.0,10.0) )
    p.add( Event(2,11.0, 16.0) )
    
    recognizer= PatternRecognizer(p, 2)
    instances = recognizer.find_signatures(s)
    print "s1, th=2: "+str(instances)
    instances = recognizer.find_signatures(s2)
    print "s2, th=2: "+str(instances)
    
    recognizer= PatternRecognizer(p, 1)
    instances = recognizer.find_signatures(s)
    print "s1, th=1: "+str(instances)