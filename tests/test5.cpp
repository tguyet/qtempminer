/**
 * test 5 : pattern recognition test !
 */

#include "QTI/qtiprefixspan.h"
#include "QTI/sequence.h"
#include "QTI/PatternRecognizer.h"
#include "IntervalSequences/IntervalEvent.h"
#include <string>
#include <sstream>
#include <vector>
#include <fstream>

using namespace std;


int main() {
	cout << ">>>>> TEST: pattern recognition <<<<<" <<endl;

	string test("1 0 1;2 1 5.4;3 1.4 2.7\n"
				"2 2.4 5.4;4 1.0 2.0;5 3.6 3.2;3 1.4 2.7;6 5.2 6.3;6 7.2 7.3\n"
				"2 2.4 5.4;4 1.0 2.0;3 1.5 2.6;6 5.2 6.3;2 2.4 5.4;4 1.0 2.0;3 1.5 2.6\n"
				"4 1.0 2.0;2 2.4 5.4;5 3.6 3.2;3 1.5 2.6\n"
				"4 1.0 2.0;2 2.4 5.4;6 5.2 6.3;2 2.4 5.4;3 1.5 2.6\n"
				"4 1.0 2.0;2 2.4 5.4;6 5.2 6.3;2 2.4 5.4;3 1.5 2.6;3 1.5 2.6\n");

	cout << ">> reading examples <<" <<endl;
	istringstream ss(test);
	IntervalSequenceFactory factory;
	vector<pair<unsigned int, sequence> > db;
	try {
		db = factory.createDatabase(ss);
	} catch(exception& e) {
		cout << "DB reading error:" << e.what() << endl;
		return 0;
	}

	cout << "===== database ===== " << endl;
	for(auto s : db) {
		cout << s.first << ":" << s.second <<endl;
	}
	cout << "====================" <<endl;



	pattern p;
	p.push_back(2);
	p.push_back(3);
	intervalFeatureSet *fs = new intervalFeatureSet();
	fs->append( new intervalFeature(0,2) );
	fs->append( new intervalFeature(1,3) );
	IntervalPatternFeature *itp = new IntervalPatternFeature(fs);
	p.setPatternFeature(itp);

	cout << "Pattern to recognize: " << p <<endl;

	PatternRecognizer reco(p, 10.0);
	PatternRecognizer::PatternOccurrence occ=reco.next(db[0].second);
	cout << "found first !" <<endl;
	cout << (PatternRecognizer::NoOccurrence == PatternRecognizer::NoOccurrence) << "/" << (PatternRecognizer::NoOccurrence != PatternRecognizer::NoOccurrence) <<endl;
	cout << (occ == PatternRecognizer::NoOccurrence) << "/" << (occ != PatternRecognizer::NoOccurrence) <<endl;

	if( reco.next(db[0].second)==PatternRecognizer::NoOccurrence) {
		cout << "not twice : ok!" <<endl;
	} else {
		cout << "not twice : ERROR !" <<endl;
	}
	reco.rewind(db[0].second);
	if( reco.next(db[0].second)!=PatternRecognizer::NoOccurrence) {
		cout << "rewind : ok!" <<endl;
	} else {
		cout << "rewind : ERROR !" <<endl;
	}

	cout << "---- alternative researches ----" << endl;
	cout << "   > research all occurrences" << endl;



	for(unsigned int i=0; i<db.size(); i++ ) {
		cout << "Process sequence " << i <<endl;
		reco.rewind(db[i].second);
		PatternRecognizer::PatternOccurrence occ=reco.next(db[i].second);
		while( occ !=  PatternRecognizer::NoOccurrence ) {
			cout << "\tfound: ";
			for(auto it : occ.occ) cout << it <<", ";
			cout << endl;
			occ=reco.next(db[i].second);
		}
	}

	return 0;
}
