#ifndef _PREFIXSPAN_H_
#define _PREFIXSPAN_H_

#include <vector>
#include <list>
#include <map>
#include <set>
#include "sequence.h"
#include "pattern.h"

/**
OUTPUT_SHOW
- 0 : no output 
- 1 : output patterns
- >1: output patterns and tid list
 */
#define OUTPUT_SHOW 1

using namespace std;



class Prefixspan {
	typedef pair<unsigned int, sequence> Transaction;

	vector<Transaction> _database; //unique database, it is not copied

	unsigned int min_sup;
	unsigned int max_pat;
	bool print_maximal_only; //options de sortie (n'améliore pas les temps de traitement !)
	pattern p;
	list< pair<pattern,unsigned int> > *pfpatterns; //! Liste de motifs fréquents (retenu pour l'interfaçage Python)

public:
	Prefixspan(unsigned int _min_sup, unsigned int _max_pat, list< pair<pattern,unsigned int> > *patlist =NULL) : min_sup(_min_sup), max_pat(_max_pat), print_maximal_only(false), pfpatterns(patlist){};

	unsigned int run();

	vector<Transaction>& database(){return _database;};

	void show_maximal(bool m=true){print_maximal_only=m;};

protected:
	//internal structures

	struct ProjDB {
		vector< pair<unsigned int, int> > indexes; //projection indexes ! Pair => sequence_id, itemset_id
		void clear() {
			indexes.clear();
		}
	};

	struct itemcount {
		set<unsigned int> transactions;		//set of the min_sup first supported transactions
		unsigned int first_found = -1;		// if first found == -1, less than min_sup transactions supports the extension, otherwise it give the index of the sequences at the end of the database for which indexes have already been constructed
		unsigned int last_transaction = -1;	// give the id of the last sequence for which an index have been added ( last_transaction == last_indexes.back().first )
		vector< pair<unsigned int, int> > last_indexes;	//indexes of the extension for the projections (computed only for the last ones)
	};

	unsigned int project(ProjDB &projected);
	void output_pattern(const ProjDB &);
};

#endif // _PREFIXSPAN_H
