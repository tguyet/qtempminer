#ifndef _PREFIXSPAN_H_
#define _PREFIXSPAN_H_

#include <vector>
#include <list>
#include <map>
#include <set>
#include "sequence.h"
#include "pattern.h"

/**
OUTPUT_SHOW
- 0 : no output 
- 1 : output patterns
- >1: output patterns and tid list
 */
#define OUTPUT_SHOW 0

using namespace std;

/**
 * \todo TODO mettre un ostream en flux de sortie pour les patterns (+ les autres sorties !)
 * \todo TODO mettre plutôt un callback pour récupérer les patterns au fur et à mesure !!
 */
class QTIPrefixspan {
	typedef pair<unsigned int, sequence> Transaction;
	sequenceFactory *_factory;

	const unsigned int max_clusters = 4;

	vector<Transaction> _database; //unique database, it is not copied

	unsigned int min_sup =1;
	unsigned int max_pat =0; //no max length if 0
	bool print_maximal_only =false; //options de sortie (n'améliore pas les temps de traitement !)
	pattern p;
	list< pattern* > *pfpatterns=nullptr; //! Liste de motifs fréquents (retenu pour l'interfaçage Python)

public:
	QTIPrefixspan(sequenceFactory *f) : _factory(f){
		//p = _factory->createPattern();
	};

	unsigned int run(unsigned int _min_sup =0, list< pattern*> *patlist =nullptr, unsigned int _max_pat =0);

	const vector<Transaction> database() const {return _database;};

	void show_maximal(bool m=true){print_maximal_only=m;};

	void readdatabase(istream &data);

	/**
	 * The sequence is copied.
	 * TODO: its id must be modified
	 *
	 * \warning{!! The sequence must have been created by the factory !!}
	 *
	 * Implemented for PythonModule purpose
	 */
	void addTransaction(const sequence &);

protected:
	//internal structures

	struct ProjDB {
		vector< pair<unsigned int, int> > indexes; //projection indexes ! Pair => sequence_id, itemset_id
		void clear() {
			indexes.clear();
		}
	};

	struct itemcount {
		set<unsigned int> transactions;		//set of the min_sup first supported transactions
		int first_found = -1;		// if first found == -1, less than min_sup transactions supports the extension, otherwise it give the index of the sequences at the end of the database for which indexes have already been constructed
		unsigned int last_transaction = -1;	// give the id of the last sequence for which an index have been added ( last_transaction == last_indexes.back().first )
		vector< pair<unsigned int, int> > last_indexes;	//indexes of the extension for the projections (computed only for the last ones)
	};

	unsigned int project(ProjDB &projected);
	unsigned int specify(ProjDB &db);
	void output_pattern(const ProjDB &);

	/**
	 * function to extract the projections of a signature
	 */
	list<featureset*> project(sequence &seq, signature &s) const;
};

#endif // _PREFIXSPAN_H
