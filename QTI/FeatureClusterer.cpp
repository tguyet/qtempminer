/*
 * IntervalClusterer.cpp
 *
 *  Created on: Aug 4, 2015
 *      Author: tguyet
 */

#include <FeatureClusterer.h>
#include <limits>
#include <iomanip>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <map>
#include <algorithm>


#include "muster/kmedoids.h"

#define M_PI 3.14159265358979323846

#define DEBUG_LEVEL 0

FeatureClusterer::FeatureClusterer() {}

FeatureClusterer::~FeatureClusterer() {}

void FeatureClusterer::setData(vector<featureset*>& d) {
	_data=d;
	_clusterid.clear();
	_centroids.clear();
}

void FeatureClusterer::cluster(unsigned int nbclusters) {
	kmedoids(nbclusters, 50);
}


unsigned int FeatureClusterer::cluster_bestk(unsigned int mink, unsigned int maxk) {
	float maxbic=-numeric_limits<float>::max();
	vector<vector<int> > bestclusterid;
	vector<int> bestcentroids;
	float bic;
	for(unsigned int k=mink; k<=maxk; k++) {
		kmedoids(k, 50, &bic);
		//cout << "\tBIC (" << k << "): " << bic << endl;
#if DEBUG_LEVEL>0
		cout << "BIC (" << k << "): " << bic << endl;
#endif
		if( bic>maxbic ) {
			maxbic=bic;
			bestclusterid = _clusterid;
			bestcentroids = _centroids;
		}
	}

	_clusterid = bestclusterid;
	_centroids = bestcentroids;

	cout << "Best BIC, " << maxbic <<", for " << _clusterid.size() << endl;
	return _clusterid.size();
}


/**
 * \brief KMedoids
 * \warning{La fonction de distance doit avoir les propriétés d'une distance pour assurer la convergence
 * de l'algorithme !!}
 */
int FeatureClusterer::kmedoids(unsigned int nclusters, int maxit, float *bic) {
	unsigned int nelements = _data.size();
	int it=0;
	int ifound=-1;
	vector<vector<int>> saved(nclusters); //solution saving for stability checking
	vector<vector<int>> bestclusters;
	vector<int> bestcentroids;

	if (nelements < nclusters) {
		return 0;
	}

	float error = std::numeric_limits<float>::max();
	vector<float> errors( nclusters );

	_clusterid.clear();
	_centroids.clear();
	_centroids.resize(nclusters);

	float besterror=std::numeric_limits<float>::max();

	unsigned int ntries=0;
	while(ntries < max_tries) { //different tries (with different starting random assignement

		if( ntries>0 && error<besterror) {
			besterror=error;
			bestclusters = _clusterid;
			bestcentroids = _centroids;
		}

		//reinit the _clusterid vector with the right size and with random cluster attributions
		randomassign(nclusters);
#if DEBUG_LEVEL>0
		cout << "clustering of " << _data.size() << " examples, of dimension " << _data[0]->dim() << " with k="<< nclusters << "." <<endl;
#endif

		while( it<maxit ) {
			float previous = error;
			error = 0.0;

			// Find the centroids from _clusterid
			getclustermedoids();
			//_centroids contains the new cluster centroids

			// Save the current cluster assignments
			saved=_clusterid;
			for(unsigned int i=0; i<_clusterid.size(); i++) {
				_clusterid[i].clear();
			}

			// reattribute examples to clusters (and compute the overall error)
			for(unsigned int i=0; i<nelements; i++) {
				// Find the closest cluster (centroid) of the example i
				float distance = std::numeric_limits<float>::max();
				int argmin=0;
				for(unsigned int icluster=0; icluster< _clusterid.size(); icluster++) {
					unsigned int j = _centroids[icluster]; //j is the id of the centroid of icluster
					if (i==j) {
						distance = 0.0;
						argmin = icluster;
						break;
					}
					float r= (*_data[i]) - (*_data[j]);
					//cout << "distance " << r <<endl;
					float tdistance = r*r;
					if (tdistance < distance) {
						distance = tdistance;
						argmin = icluster;
					}
				}
				_clusterid[argmin].push_back(i); //add the example to the best cluster!
				error += distance*distance;
				errors[argmin]+=distance*distance;
			}
#if DEBUG_LEVEL>1
			cout << "\t"<< setprecision(3) << "iter " << it << ", error "<< error << "."<< endl;
			unsigned int d=0;
			for(auto l : _clusterid) {
				d+=l.size();
			}
			cout << "\tclassified examples " << d <<"/"<< _data.size() <<endl;
#endif

			if (error>=previous) {
#if DEBUG_LEVEL>0
				cout << setprecision(3) << "iter " << it << ", error "<< error <<" does not improve the previous step ("<<previous<<")."<<endl;
#endif
				break;
			}

			if( saved==_clusterid ) {
#if DEBUG_LEVEL>0
				cout << "iter " << it << ": converged!"<<endl;
#endif
				break;
			}

#if DEBUG_LEVEL>0
			cout << setprecision(3) << "iter " << it << ", error "<< error <<"."<<endl;
#endif

			it++;
		}
		ntries++;
	}

	_clusterid=bestclusters;
	_centroids=bestcentroids;

	//BIC Criterion
	if(bic!=nullptr) {
		/*
		if( nclusters == nelements ) {
		 *bic=numeric_limits<float>::max();
		} else {
			float sigma =0;
			//for(auto e : errors) sigma += e;
			//sigma /= (float)(nelements-nclusters);
			sigma=error/(float)(nelements-nclusters); //variance


		 *bic=0;
			for(unsigned int id=0; id<_clusterid.size(); id++) {
				unsigned int n = _clusterid[id].size();
				//TODO améliorer le critère BIC avec le calcul de la matrice de covariance ?
				//float score = n*(log(n) - log(nelements) - log(2.0 * M_PI)*.5 - _data[0]->dim()*log(sigma)*.5 ) - (n-nclusters)*.5;
				float score = n*( log(n/nelements) - _data[0]->dim()*log(2.0 * M_PI*sigma)*.5 - errors[id]/(2.0*sigma));
		 *bic+=score;
			}
		}
		 */
		*bic=-error;
	}

	return ifound;
}


void FeatureClusterer::randomassign(unsigned int nclusters) {
	int k = 0;
	int j;
	float p;
	int n = _data.size()-nclusters;

	// Create a random permutation of the cluster assignments
	std::vector<int> permutation(_data.size());
	for(unsigned int i=0; i<_data.size(); ++i) permutation[i]=i;
	std::random_shuffle ( permutation.begin(), permutation.end() );

	_clusterid.clear();
	_clusterid.resize(nclusters);

	/* Draw the number of elements in each cluster from a multinomial
	 * distribution, reserving ncluster elements to set independently
	 * in order to guarantee that none of the clusters are empty.
	 */
	for(unsigned i = 0; i < nclusters-1; i++) {
		_clusterid[i].clear();
		p = 1.0/(nclusters-i);
		j = binomial(n, p);
		n -= j;
		j += k+1; // Assign at least one element to cluster i
		for(; k<j; k++)
			_clusterid[i].push_back( permutation[k] );
	}
	// Assign the remaining elements to the last cluster
	for(; k < (int)_data.size(); k++) _clusterid[nclusters-1].push_back( permutation[k] );

#if DEBUG_LEVEL>1
	cout << " ---- assignement ---"<<endl;
	for(auto v:_clusterid) {
		for(auto i : v) {
			cout << i << ",";
		}
		cout << endl;
	}
	cout << " --------------------"<<endl;
#endif
}

/**
 * Compute the centroid for each cluster as the example that minimizes the intra-cluster distances
 */
void FeatureClusterer::getclustermedoids() {
	for(unsigned int j=0; j< _clusterid.size(); j++) {//j is the id of the cluster
		float error = numeric_limits<float>::max();
		for(auto k : _clusterid[j]) {//k: example to test as a new centroid
			featureset *centroid=_data[k];
			//compute the sum of the distances of elements of j to the example i
			float d = 0.0;
			for(auto i : _clusterid[j]) {
				if (i==k) continue;
				float r= (*centroid) - (*_data[i]);
				d += r*r;
				if( d>error ) break;
			}
			if( d<error ) {
				error = d;
				_centroids[j] = k;
			}
		}
	}
}



/**
This routine returns a uniform random number between 0.0 and 1.0. Both 0.0
and 1.0 are excluded. This random number generator is described in:

Pierre l'Ecuyer
Efficient and Portable Combined Random Number Generators
Communications of the ACM, Volume 31, Number 6, June 1988, pages 742-749,774.

The first time this routine is called, it initializes the random number
generator using the current time. First, the current epoch time in seconds is
used as a seed for the random number generator in the C library. The first two
random numbers generated by this generator are used to initialize the random
number generator implemented in this routine.

\return A float-precison number between 0.0 and 1.0.
 */
float FeatureClusterer::uniform(){
	int z;
	static const int m1 = 2147483563;
	static const int m2 = 2147483399;
	const float scale = 1.0/m1;

	static int s1 = 0;
	static int s2 = 0;

	if (s1==0 || s2==0) {/* initialize */
		unsigned int initseed = (unsigned int) time(0);
		srand(initseed);
		s1 = rand();
		s2 = rand();
	}

	do {
		int k;
		k = s1/53668;
		s1 = 40014*(s1-k*53668)-k*12211;
		if (s1 < 0) s1+=m1;
		k = s2/52774;
		s2 = 40692*(s2-k*52774)-k*3791;
		if(s2 < 0) s2+=m2;
		z = s1-s2;
		if(z < 1) z+=(m1-1);
	} while (z==m1); /* To avoid returning 1.0 */

	return z*scale;
}

/**
This routine generates a random number between 0 and n inclusive, following
the binomial distribution with probability p and n trials. The routine is
based on the BTPE algorithm, described in:

Voratas Kachitvichyanukul and Bruce W. Schmeiser:
Binomial Random Variate Generation
Communications of the ACM, Volume 31, Number 2, February 1988, pages 216-222.

\param  p The probability of a single event. This probability should be less than or
equal to 0.5.
\param n The number of trials.
\return An integer drawn from a binomial distribution with parameters (p, n).
 */
int FeatureClusterer::binomial(int n, float p) {
	const float q = 1 - p;
	if (n*p < 30.0) {/* Algorithm BINV */
		const float s = p/q;
		const float a = (n+1)*s;
		float r = exp(n*log(q)); /* pow() causes a crash on AIX */
		int x = 0;
		float u = uniform();
		while(1) {
			if (u < r) return x;
			u-=r;
			x++;
			r *= (a/x)-s;
		}
	} else {/* Algorithm BTPE */
		/* Step 0 */
		const float fm = n*p + p;
		const int m = (int) fm;
		const float p1 = floor(2.195*sqrt(n*p*q) -4.6*q) + 0.5;
		const float xm = m + 0.5;
		const float xl = xm - p1;
		const float xr = xm + p1;
		const float c = 0.134 + 20.5/(15.3+m);
		const float a = (fm-xl)/(fm-xl*p);
		const float b = (xr-fm)/(xr*q);
		const float lambdal = a*(1.0+0.5*a);
		const float lambdar = b*(1.0+0.5*b);
		const float p2 = p1*(1+2*c);
		const float p3 = p2 + c/lambdal;
		const float p4 = p3 + c/lambdar;
		while (1) {
			/* Step 1 */
			int y;
			int k;
			float u = uniform();
			float v = uniform();
			u *= p4;
			if (u <= p1) return (int)(xm-p1*v+u);
			/* Step 2 */
			if (u > p2) { /* Step 3 */
				if (u > p3) { /* Step 4 */
					y = (int)(xr-log(v)/lambdar);
					if (y > n) continue;
					/* Go to sample_rate 5 */
					v = v*(u-p3)*lambdar;
				} else { y = (int)(xl+log(v)/lambdal);
				if (y < 0) continue;
				/* Go to sample_rate 5 */
				v = v*(u-p2)*lambdal;
				}
			} else {
				const float x = xl + (u-p1)/c;
				v = v*c + 1.0 - fabs(m-x+0.5)/p1;
				if (v > 1) continue;
				/* Go to sample_rate 5 */
				y = (int)x;
			}
			/* Step 5 */
			/* Step 5.0 */
			k = abs(y-m);
			if (k > 20 && k < 0.5*n*p*q-1.0) { /* Step 5.2 */
				float rho = (k/(n*p*q))*((k*(k/3.0 + 0.625) + 0.1666666666666)/(n*p*q)+0.5);
				float t = -k*k/(2*n*p*q);
				float A = log(v);
				if (A < t-rho) return y;
				else if (A > t+rho) continue;
				else { /* Step 5.3 */
					float x1 = y+1;
					float f1 = m+1;
					float z = n+1-m;
					float w = n-y+1;
					float x2 = x1*x1;
					float f2 = f1*f1;
					float z2 = z*z;
					float w2 = w*w;
					if (A > xm * log(f1/x1) + (n-m+0.5)*log(z/w)
							+ (y-m)*log(w*p/(x1*q))
							+ (13860.-(462.-(132.-(99.-140./f2)/f2)/f2)/f2)/f1/166320.
							+ (13860.-(462.-(132.-(99.-140./z2)/z2)/z2)/z2)/z/166320.
							+ (13860.-(462.-(132.-(99.-140./x2)/x2)/x2)/x2)/x1/166320.
							+ (13860.-(462.-(132.-(99.-140./w2)/w2)/w2)/w2)/w/166320.)
						continue;
					return y;
				}
			} else { /* Step 5.1 */
				int i;
				const float s = p/q;
				const float aa = s*(n+1);
				float f = 1.0;
				for (i = m; i < y; f *= (aa/(++i)-s));
				for (i = y; i < m; f /= (aa/(++i)-s));
				if (v > f) continue;
				return y;
			}
		}
	}
	/* Never get here */
	return -1;
}


////////////////////////////////////////////////////////////////////////

void FeatureClusterer::cluster_xpam() {

	_clusterid.clear();
	_centroids.clear();

	//Dissimilarity matrix construction
	unsigned int N=_data.size();
	cluster::dissimilarity_matrix mat(N);
	for (size_t i=0; i < N; i++) {
		for (size_t j=0; j <= i; j++) {
			mat(i,j) = (*_data[i]) - (*_data[j]);
		}
	}
	cluster::kmedoids km;
	unsigned int dimensions =  _data[0]->dim();

	unsigned int max_nbclust=10;

	km.xpam(mat, min(max_nbclust,N), dimensions);
	/**
	 * TODO: modifier le code de clustering pour supprimer les utilisations de boost !
	 */

	//Get the medoids and the clusters descriptions
	unsigned int nbc = km.num_clusters();
	_clusterid.resize(nbc);
	_centroids.resize(nbc);
	map<int, int> reverseids;

	for(unsigned int i=0; i< nbc; i++) {
		_centroids.push_back( km.medoid_ids[i]);
		reverseids.insert( make_pair(km.medoid_ids[i], i) );
	}

	unsigned int i=0;
	for(auto fids : km.cluster_ids) {
		vector<int> ids;
		_clusterid[ reverseids[fids]  ].push_back(i);
		i++;
	}
}

