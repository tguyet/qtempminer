/*
 * PatternRecognizer.h
 *
 *  Created on: 10 oct. 2015
 *      Author: tguyet
 */

#ifndef PATTERNRECOGNIZER_H_
#define PATTERNRECOGNIZER_H_

#include <list>
#include <map>
#include "sequence.h"
#include "pattern.h"

using namespace std;

/**
 * a class that is dedicated to the localisation of patterns in a sequence
 * TODO :
 * 	Cette class n'est pas terminée d'implémenter !!
 * 		-> le problème est de retrouver les patterns dasn une longue séquence ... et formellement, une longue séquence n'est pas une séquence comme
 * 		celle des bases de données -> il faut prendre en considération le décalage temporel entre les éléments
 * 		=> la conséquence, c'est qu'il faut distinguer un feature (caractéristiques d'un événement dans la base de séquences ou dans les motifs)
 * 		d'un feature d'un événément dans une séquences qui serait obtenu (par extraction avec fenêtres) -> le second peut matcher le premier "à un décalage près"
 *
 * 		Au final, le recognizer ne doit pas être mis dans QTI parce qu'il n'y est quetsion que de bases de séquences !
 * 		-> ca place est dans l'outillage relatif aux longues séquences (module encore inexistant !)
 *
 * 		NB: les algos restent à vérifier, mais la direction est bonne !!
 */
class PatternRecognizer {
protected:
	const pattern &_pat;
	double _threshold =0.0;

public:
	/**
	 * A pattern occurrence
	 */
	struct PatternOccurrence {
		int seqid =-1;
		double time_decay =0;
		list<unsigned int> occ;
	};

	//HACK: it would be better to have it as a const static ...
	static PatternOccurrence NoOccurrence;

	PatternRecognizer(const pattern &pat, double t=0.0);
	virtual ~PatternRecognizer();

	/**
	 * get next pattern occurrence
	 * If none, an emptu PatternOccurrence is returned
	 */
	PatternOccurrence next(sequence &seq);
	void rewind(sequence &seq);

protected:

	/**
	 * for each sequence we keep an current search index + currently recognized events
	 */
	struct sequence_info {
		unsigned int reading_index;
		/**> ongoing_recognized collect partially recognize instances of the pattern _pat
		 * (i, pos) indicates that there is an partial instance where position pos corresponds to the ith event of the pattern
		 */
		list< pair<unsigned int, PatternOccurrence> > ongoing_recognized;
	};
	map<sequence*, sequence_info> _current_indexes;
};

bool operator==(const PatternRecognizer::PatternOccurrence &occ1, const PatternRecognizer::PatternOccurrence &occ2);
bool operator!=(const PatternRecognizer::PatternOccurrence &occ1, const PatternRecognizer::PatternOccurrence &occ2);



#endif /* PATTERNRECOGNIZER_H_ */
