
#include "QTI/qtiprefixspan.h"
#include "QTI/sequence.h"
#include "IntervalSequences/IntervalEvent.h"
#include <string>
#include <sstream>
#include <vector>

using namespace std;

int main() {
	cout << ">>>>> TEST: basic prefix span test <<<<<" <<endl;

	string test("1 2.1 3.1;2 2.3 5.6;3 1.5 2.6\n"
			"4 1.0 2.0;5 3.6 3.2;6 5.2 6.3;6 7.2 7.3\n"
			"4 1.0 2.0;2 2.3 5.6;3 1.5 2.6;6 5.2 6.3");

	cout << ">> reading examples <<" <<endl;
	istringstream ss(test);
	IntervalSequenceFactory factory;

	QTIPrefixspan ps(&factory);
	ps.readdatabase(ss);

	cout << "===== database ===== " << endl;
	for(auto s : ps.database()) {
		cout << s.first << ":" << s.second <<endl;

	}
	cout << "====================" <<endl;

	cout << ">> running prefixspan <<" <<endl;
	ps.run(/*fmin*/2, nullptr, /*lenmax*/1);

	return 0;
}
