/*
 * IntervalEvent.cpp
 *
 *  Created on: Aug 4, 2015
 *      Author: tguyet
 */

#include "IntervalEvent.h"
#include "QTI/pattern.h"
#include <iomanip>
#include <cmath>

IntervalComparator *intervalFeature::IntervalCmp = new IntervalEuclidiean();

intervalFeature::intervalFeature(double s, double f) {
	set(s,f);
}

void intervalFeature::set(double s, double f) {
	if(s>f) {
		I = Interval(f,s);
	} else {
		I = Interval(s,f);
	}
}

bool intervalFeature::operator==(const feature & f) const {
	intervalFeature &intf=(intervalFeature&)f; //TODO : catching errors !
	return I==intf.I;
}

float intervalFeature::operator-(const feature & f) const {
	const intervalFeature intf = static_cast<const intervalFeature &>(f);
	return (*IntervalCmp)(intf.I,this->I);
}

void intervalFeature::print(ostream &os) const {
	os << I;
}

////////////////////////////////////////////////////////////////

intervalFeatureSet::intervalFeatureSet():featureset() {
	cmp=new Haussdorf();
}

intervalFeatureSet::intervalFeatureSet(const intervalFeatureSet &fs):featureset(fs) {
	cmp=new Haussdorf();
}

float intervalFeatureSet::operator-(const featureset &fs) const {
	return (*cmp)(*this, static_cast<const intervalFeatureSet &>(fs));
}

featureset *intervalFeatureSet::clone() const {
	return new intervalFeatureSet(*this);
}

void intervalFeatureSet::append(feature *fs) {
	featureset::append(fs);
}

intervalFeature & intervalFeatureSet::interval(unsigned int p) {
	return static_cast<intervalFeature &>( *_features[p]);
}


const intervalFeature & intervalFeatureSet::interval(unsigned int p) const {
	return static_cast<const intervalFeature &>( *_features[p]);
}

////////////////////////////////////////////////////////////////

double IntervalSetEuclidiean::operator()(const intervalFeatureSet &I,const intervalFeatureSet &I2) {
	double d=0;
	if( I.dim() != I2.dim() ) {
		return d;
	}
	for(unsigned int i=0; i<I.dim(); i++) {
		double r=I[i].s()-I2[i].s();
		double s=I[i].f()-I2[i].f();
		d += sqrt(r*r+s*s);
	}
	return d;
}


double CityBlock::operator()(const intervalFeatureSet &I, const intervalFeatureSet &I2) {
	double d=0;
	if(I.dim() != I2.dim()) {
		return d;
	}
	for(unsigned int i=0; i<I.dim(); i++) {
		double r=I[i].f()-I2[i].f();
		double s=I[i].s()-I2[i].s();
		d += abs(r)+abs(s);
	}
	return d;
}

double Haussdorf::operator()(const intervalFeatureSet &I, const intervalFeatureSet &I2) {
	double d=0;
	if(I.dim() != I2.dim()) {
		return d;
	}
	for(unsigned int i=0; i<I.dim(); i++) {
		double r=I[i].f()-I2[i].f();
		double s=I[i].s()-I2[i].s();
		d += std::max(abs(r),abs(s));
	}
	return d;
}

double GowdaDiday::operator()(const intervalFeatureSet &I, const intervalFeatureSet &I2) {
	double d=0;
	if(I.dim() != I2.dim()) {
		return d;
	}

	for(unsigned int i=0; i<I.dim(); i++) {
		//Dp
		double val = I[i].duration() + I2[i].duration();
		d+= abs( I[i].s()-I2[i].s() )/val;
		//Ds
		val = std::max(I[i].f(),I2[i].f()) - std::min(I[i].s(),I2[i].s());
		d += abs(I[i].duration()-I2[i].duration())/val;
		//Dc
		d += ( I[i].duration()+I2[i].duration() - 2* (std::max(0.0, std::min(I[i].f(),I2[i].f())) - std::max(I[i].s(),I2[i].s())) )/val;
	}
	return d;
}

////////////////////////////////////////////////////////////////

void IntervalPatternFeature::print(ostream& os) const {
	if(_fs) {
		os << "{" << *_fs << " }";
	} else {
		os << "[none]";
	}
}

patternfeature *IntervalPatternFeature::clone() const {
	IntervalPatternFeature *p=new IntervalPatternFeature();
	p->_fs = static_cast<intervalFeatureSet *>( this->_fs->clone() );
	return p;
}

////////////////////////////////////////////////////////////////


event IntervalSequenceFactory::createEvent(istream &str) const {
	event e=sequenceFactory::createEvent();
	double s,dur;
	eventtype id;
	str >> id;
	str >> s ;
	str >> dur;
	if ( str.fail() ) {
		throw string("reading error: invalid event\n");
	}
	if( !str.eof() ) {
		throw string("reading error: delimiter error (expected '")+string(1,eventdelimiter)+string("').\n");
	}
	setId(e,id);
	setFeature(e, new intervalFeature(s,s+dur));
	return e;
}

featureset *IntervalSequenceFactory::createFeature(event &e) const {
	intervalFeature *ifeat=(intervalFeature*)(&e.f());
	intervalFeatureSet *ifs=new intervalFeatureSet();
	ifs->append(ifeat);
	return ifs;
}

featureset *IntervalSequenceFactory::createFeature(list<event*> &le) const {
	intervalFeatureSet *ifs=new intervalFeatureSet();
	for(auto e : le) {
		intervalFeature*ifeat=(intervalFeature*)(&e->f());
		ifs->append(ifeat);
	}
	return ifs;
}

patternfeature *IntervalSequenceFactory::createPatternFeature(const featureset *center,  list<const featureset*> &instances) const {
	const intervalFeatureSet* c = static_cast<const intervalFeatureSet *>( center);
	IntervalPatternFeature *pf = new IntervalPatternFeature(c);
	return pf;
}


pattern *IntervalSequenceFactory::createPattern() {
	pattern *p = sequenceFactory::createPattern();
	p->setPatternFeature( new IntervalPatternFeature(new intervalFeatureSet()) );
	return  p;
}
