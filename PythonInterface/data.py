# -*- coding: utf-8 -*-
"""
author: T. Guyet
date: 09/2015
"""
from copy import deepcopy
import pyqtgraph as pg
import numpy as np
import string

#modules required for load/save databases
import pickle
import gzip

class Timeserie:
    """
    A class for representing time series
    TODO: explicit time representation !
    """
    
    def __init__(self, filename =""):
        self.data=None
        self.__mean = 0
        self.__var = 0
        self.__center = False
        if len(filename)>0:
            try:
                vals=list()
                fin = open(filename,"r")
                for line in fin:
                    try:
                        vals.append( float(line) )
                    except ValueError:
                        pass
                self.data = np.array( vals )
                self.__mean = np.mean(self.data)
                self.__var = np.var(self.data)
            except IOError:
                print 'Cannot open file : %s' % filename
        

    def center(self):
        """
        Center and reduce the time series (values will be between -1 and 1)
        """
        if self.__center:
            return
        tc = self.data - self.__mean
        self.data=tc/np.sqrt(np.var(tc))
        self.__center = True
        
'''        
class Dataset:
    """
    A class for a dataset of time series
    """
    
    def __init__(self):
        self.timeseries=list()
        
    def add(self, ts):
        """
        add a new time series
        """
        self.timeseries.append(ts)

    def get(self, i):
        """
        Getter for a time series
        """
        return self.timeseries[i]
'''

class EventInfo:
    def __init__(self):
        self.name=""        # the string describing the event
        self.descr=""        # a small escription of the event
        #some drawing parameters
        self.bgcolor=(0,0,0,0)     # a color is a tuple of 4 integers between 0 and 255
        self.fgcolor=(0,0,0,0)     # a color is a tuple of 4 integers between 0 and 255
        self.default_pos=0        # the drawing y position of the event
        self.default_high=1        # the drawing high of the event

class EventMap:
    """
    A class to represent a map of event.
    """    
    def __init__(self):
        self.events=dict()
    
    def add(self, ide):
        """
        default addition of an event in the eventmap
        """
        if not (ide in self.events):
            #print "add " +str(ide)
            self.events[ide]=EventInfo()
        
    def items(self):
        return self.events.keys()
        
    def addinfo(self, id, info):
        if not isinstance(info, EventInfo):
            print("EventMap::add error")
            return None
            
        if id in self.events:
            print("The event already exists in the map")
            return None
        self.events[id]=info
        
        
    def generateColors(self):
        """
        Generate the colors of the events
        """
        #cmap_bw = pg.ColorMap([0,len(self.events)], [(0,0,0,255), (255,255,255,255)])
        pos = np.array([0, len(self.events), int(0.5*len(self.events)), int(0.25*len(self.events)), int(0.75*len(self.events))])
        color = np.array([[0,255,255,255], [255,255,0,255], [0,0,0,255], (0, 0, 255, 255), (255, 0, 0, 255)], dtype=np.ubyte)
        cmap = pg.ColorMap(pos, color)
        i=0
        for (k,v) in self.events.iteritems():
            #print cmap.getColors()
            v.bgcolor=cmap.map(i)
            i+=1
            
    def getInfos(self,id):
        if not (id in self.events):
            return None
        return self.events[id]
        

class Event:
    """
    A class to represent an event
    """
    def __init__(self, s =0, b =0, e =0):
        self.id=s
        self.begin=min(float(b), float(e))
        self.end=max(float(b), float(e))
        
    def plot(self, plotitem, emap):
        """
        A function to draw an event of event in a plotitem (see pyqtgraph library)
        """
        infos = emap.getInfos(self.id)
        if infos is None:
            print "infos error with " + str(self.id)
        else:
            x=np.array([self.begin,self.end,self.end,self.begin,self.begin])
            y=np.array([infos.default_pos,infos.default_pos,infos.default_pos+infos.default_high,infos.default_pos+infos.default_high,infos.default_pos])            
            plotitem.plot(x, y, fillLevel=0, brush=infos.bgcolor, pen=infos.fgcolor)
        
    def __repr__(self):
        return "%s [%s, %s]" % (self.id, self.begin, self.end)

    def ___str__(self):
        return self.__repr__()
        
    def asTuple(self):
        return (self.id, self.begin, self.end)
        
        
    def __deepcopy__(self, memo):
        e = Event()
        e.id=self.id
        e.begin=self.begin
        e.end=self.end
        return e
        
    def __copy__(self, memo):
        e = Event()
        e.id=self.id
        e.begin=self.begin
        e.end=self.end
        return e
        
    def __cmp__ ( self, x ):
        
        if isinstance(x,Event) :
            if(self.begin<x.begin):
                return -1
            elif(self.begin>x.begin):
                return 1
            else:
                if(self.end<x.end):
                    return -1
                elif(self.end>x.end):
                    return 1
                else:
                    if(self.id<x.id):
                        return -1
                    elif(self.id>x.id):
                        return 1
                    else:
                        return 0
        elif isinstance(x,int):
            if(self.id<x):
                return -1
            elif(self.id>x):
                return 1
            else:
                return 0
        else:
            raise ValueError('Impossible to compare an Event with the proposed object')


class SequenceIterator:
    def __init__(self, seq):
        self.i = -1
        self.seq=seq
    
    def __iter__(self):
        return self
        
    def next(self):
        self.i = self.i + 1
        if self.i >= len(self.seq):
            raise StopIteration
        return self.seq[self.i]
    

class Sequence:
    """
    A class to represent sequences of events
    """
    def __init__(self):
        self.seq = list()
        self.emap = EventMap()
        
    def add(self, e, sort=False):
        """
        Add an event to the sequence and update the event map with default informations        
        @param e an event or a list/tuple of events to add to the sequence
        @param sort force sorting the whole sequence if true (default false)
        """
        if isinstance(e, Event):
            self.seq.append(e)
            self.emap.add(e.id)
            if sort:
                self.sort()
        elif isinstance(e, list) or isinstance(e, tuple):
            for ee in e:
                self.add(ee, False)
            if sort:
                self.sort()
        else:
            print("Sequence::add: wrong type for e")
            return None

    def itemsList(self):
        return self.emap.items()
        
    def autoConfig(self):
        self.emap.generateColors()
        for k,v in self.emap.events.iteritems():
            v.default_pos = k
        
    def sort(self):
        """Sort the sequence according to the __cmp__ operator of events
        """
        self.seq.sort()
            
    def plot(self, plotitem):
        """a function to draw a sequence of event in a plotitem (see pyqtgraph library)
        """
        i=0
        for e in self.seq:
            i+=1
            if i > 100:
                break
            #print("plot "+str(e))            
            e.plot( plotitem, self.emap )

    def __repr__(self):
        s=""
        for e in self.seq:
            s+= str(e) + " > "
        return s

    def ___str__(self):
        return self.__repr__()
        
    def __len__(self):
        return len(self.seq)
        
    def __getitem__(self, k):
        return self.seq[k]
        
    def __contains__(self, obj):
        return obj in self.seq

    """
    functions for iteration over a sequence
    """
    def __iter__(self):
        return SequenceIterator(self)

    def extract(self, wbegin, wend):
        """
        @brief the function extracts the subsequence of the events that are between the positions wbin and wend.
        An event is in the subsequence if the intersection of its interval with the windows is not empty.
        @param wbegin, wend: limit of the intervals
        The sequence is assumed to be sorted !
        The function creates a new sequence (deep copy). The event map is not copied.
        """
        S = Sequence()
        for e in self.seq:
            if( e.begin>wend ):
                break #finished
            if e.end<wbegin:
                continue
            S.add( deepcopy(e), False)
        return S
        
    def find(self, id):
        """ @brief The function extracts the subsequence of the events that are of the id type
        The function creates a new sequence (deep copy). The event map is not copied.
        """
        S = Sequence()
        for e in self.seq:
            if( e.id==id ):
                S.add( deepcopy(e), False)
        return S
        
    def translate(self, decay):
        """@brief temporal translation of the events timestamps
        """
        for e in self.seq:
            e.begin = e.begin  + decay
            e.end = e.end  + decay

class Dataset_iterator:
    def __init__(self, ds):
        self.i=0
        self.ds=ds
        
    def __iter__(self):
        return self
    
    def next(self):
        if self.i>=len(self.ds.db):
            raise StopIteration
        else:
            s=self.ds.db[self.i]            
            self.i+=1
            return s

class Dataset:
    def __init__(self):
        self.db=[]
        
    def __iter__(self):
        return Dataset_iterator(self)
        
    def __len__(self):
        return len(self.db)
        
    def __getitem__(self, i):
        return self.db[i]
    
    def itemsList(self):
        l = set()
        for s in self:
            for a in s.itemsList():
                l.add( a )
        l = list(l)
        l.sort()
        return l
    
    def add(self, seq):
        """ add a sequence or a list of sequences to the database
        sequence must be objects of the SubSequence class
        """
        if isinstance(seq, list) or isinstance(seq,tuple):
            for s in seq:
                self.add(s)
        else:
            if not isinstance(seq,Sequence):
                print("Dataset::add: only Sequence are possible")
                return False
            self.db.append(seq)
            
    def save(self, filename, bin = 1):
        """Saves a compressed object to disk
        """
        file = gzip.GzipFile(filename, 'wb')
        file.write(pickle.dumps(self, bin))
        file.close()
        
def loadDataset(filename):
    """Loads a compressed dataset from disk
        It is a global function !
    """
    file = gzip.GzipFile(filename, 'rb')
    buffer = ""
    while 1:
        data = file.read()
        if data == "":
            break
        buffer += data
    db = pickle.loads(buffer)
    file.close()
    return db

def loadCsvDataset(filename, maxlen=-1):
    """Loads a csv dataset from disk
        It is a global function !
    """
    ds = Dataset()
    fin = open(filename, "r")
    for line in fin:
        seq = Sequence()
        elems=string.split(line, sep=" ")
        i=1 #on commence à 1 parce qu'il y a le numéro de transaction
        while i<(len(elems)-1) and (maxlen==-1 or i<maxlen):
            try:
                s=int(elems[i])
                i+=1
                begin=long(elems[i])
                i+=1
                end=long(elems[i])
                i+=1
                e=Event(s,begin,end)
                seq.add(e,False)
            except ValueError:
                print("reading error {0}/{2}: '{1}'".format(i, elems[i],len(elems)))
                break
                #return None
        seq.sort()
        ds.add(seq)
    fin.close()
    return ds


if __name__ == '__main__':
    #########################   
    ### Some simple tests ###
    #########################
        
    s=Sequence()
    
    s.add( Event(1,24.0,35) )
    s.add( Event(2,34.0,45.0) )
    s.add( Event(3,56.0,67.0) )
    s.add( Event(2,12.0,20.0) )
    
    print("--- test add a list of events ---")
    l=list()
    l.append(Event(5,34,45))
    l.append(Event(6,24,45))
    s.add(l, True)
    
    print("--- test printing and iterators ---")
    for e in s:
        print e
    print(s)
    print (s[2])
    
    print("--- test of operators ---")
    #   -> the test uses the __cmp__ function on Events (works with )
    print(s[2] in s)
    print(3 in s)
    print(7 in s)
    
    
    print("--- test deep copy ---")
    e = s[4]            # shallow copy
    de = deepcopy(s[2]) # deep copy
    e.id=10
    de.id=11
    print s
    
    print("--- test extract ---")
    s2=s.extract(31,55)
    print s2
    
    s3=s.find(2)
    print s3
    
    print("--- test pickle ---")  
    print("  > dump/load simple")
    output = open("sequence.pkl","wb")
    pickle.dump(s, output)
    output.close()
    
    finput = open("sequence.pkl","rb")
    sin = pickle.load(finput)
    finput.close()
    print(sin)
    
    print("  > dump/load dataset")
    ds=Dataset()
    ds.add(s)
    ds.save("db.pkl")
    
    ds2=loadDataset("db.pkl")
    for s in ds2:
        print s
    
    
    