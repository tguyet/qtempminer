/*
 * Pattern.cpp
 *
 *  Created on: Aug 3, 2015
 *      Author: tguyet
 */

#include <exception>

#include "pattern.h"
#include "sequence.h"

pattern::pattern() {}

pattern::pattern(const pattern &p) {
	cerr << "pattern copy : not implemented !" << endl;
	_pf=nullptr;
}


pattern::~pattern() {
	if(_pf) delete(_pf);
}

pattern *pattern::clone() const {
	pattern *p=new pattern();
	p->setSupport(_support);
	p->_base=_base;
	if( _pf ) p->_pf = _pf->clone();
	return p;
}


ostream& operator<<(ostream &os, const pattern &p) {
	for(auto e : p._base) {
		os << e << ",";
	}
	if( p._pf) {
		os << *p._pf;
	}

	return os;
}

signature pattern::getSignature() const {
	signature s;
	s.insert(s.end(), _base.begin(), _base.end());
	return s;
}

const feature *pattern::getFeature(unsigned int pos) const {
	if( !_pf) return nullptr;
	if ( pos<0 || pos>=_base.size()) {
		//What should we return in case of out of bounds errors ??
		throw std::logic_error("access out of feature size");
	}
	return &((*_pf)[pos]);
}

eventtype pattern::getSignature(unsigned int i) const {
	if ( i<0 || i>=_base.size()) {
		//What should we return in case of out of bounds errors ??
		throw std::logic_error("access out of feature size");
	}
	return _base[i];
};
