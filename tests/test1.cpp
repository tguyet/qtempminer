
#include "QTI/sequence.h"
#include <string>
#include <sstream>
#include <vector>

using namespace std;

int main() {
	cout << ">>>>> TEST: basic sequence factory <<<<<" <<endl;

	//// Test 1
	cout << ">> test1: simple reading" << endl;
	string test("1,2,3\n4,5,6\n1,3\n1,5,4,3");
	cout << "===== input ===== " << endl;
	cout << test <<endl;
	cout << "=================" <<endl;

	cout << ">> reading example <<" <<endl;
	istringstream ss(test);
	sequenceFactory factory;
	vector<pair<unsigned int, sequence>> db= factory.createDatabase(ss);

	cout << "===== output ===== " << endl;
	for(auto s : db) {
		cout << s.first << ":" << s.second <<endl;

	}
	cout << "==================" <<endl;

	//// Test 2
	cout << ">> test2: reading error (type error)" << endl;
	test = "1,2,3\n4,a,6\n";
	cout << "===== input ===== " << endl;
	cout << test <<endl;
	cout << "=================" <<endl;

	cout << ">> reading example <<" <<endl;

	istringstream ss2(test);
	db.clear();
	try{
		db= factory.createDatabase(ss2);
	} catch(exception &e) {
		cerr << e.what();
	} catch ( const std::string & Msg ) {
	    std::cerr << Msg;
	}

	cout << "===== output ===== " << endl;
	for(auto s : db) {
		cout << s.first << ":" << s.second <<endl;

	}
	cout << "==================" <<endl;

	//// Test 3
	cout << ">> test3: reading error (comma error)" << endl;
	test = "1,2;3\n4;5;6\n";
	cout << "===== input ===== " << endl;
	cout << test <<endl;
	cout << "=================" <<endl;

	cout << ">> reading example <<" <<endl;

	istringstream ss3(test);
	db.clear();
	try{
		db= factory.createDatabase(ss3);
	} catch(exception &e) {
		cerr << e.what();
	} catch ( const std::string & Msg ) {
	    std::cerr << Msg;
	}

	cout << "===== output ===== " << endl;
	for(auto s : db) {
		cout << s.first << ":" << s.second <<endl;

	}
	cout << "==================" <<endl;

	return 0;
}
