
#include "QTI/qtiprefixspan.h"
#include "QTI/sequence.h"
#include <string>
#include <sstream>
#include <vector>

using namespace std;

int main() {
	cout << ">>>>> TEST: basic prefix span test <<<<<" <<endl;

	string test("1,2,3\n4,5,6\n1,3\n1,5,4,3,5");

	cout << ">> reading example <<" <<endl;
	istringstream ss(test);
	sequenceFactory factory;

	QTIPrefixspan ps(&factory);
	ps.readdatabase(ss);

	cout << "===== database ===== " << endl;
	for(auto s : ps.database()) {
		cout << s.first << ":" << s.second <<endl;

	}
	cout << "====================" <<endl;

	cout << ">> running prefixspan <<" <<endl;
	ps.run(/*fmin*/2, nullptr,  /*lenmax*/3);

	return 0;
}
