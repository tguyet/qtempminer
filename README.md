# QTempMiner

QTempMiner is an API that implement the mining of sequences of events timestamped with intervals. 
It extracts the typical patterns frequently occurring in a set of sequences.