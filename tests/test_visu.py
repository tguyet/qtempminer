#!/usr/bin/python
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from prefixspan_module import *


data2=[  [ (6,1.2,2.3), (2,5.2,5.6)],
        [ (1,0.6,1.2), (2,2.6,2.9), (3,3.6,5.8)],
        [ (1,2.3,3.1),(3,2.9,5.6),(5,4.5,4.9), (2,4.6,5.9),(4,5.6,7.2)],
        [ (2,3.4, 10.2), (1,3.9, 8.2),(4, 4.2,4.5)],
        [ (1,0.2,1.5),(3,1.2,1.8),(5,2.5,5.6) ]]
#print "data:", data2

minval=1
maxval=6

jet = plt.get_cmap('jet')
cNorm  = colors.Normalize(vmin=0, vmax=6)
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

fig, ax = plt.subplots()
i=0
for sequence in data2:
	intervals=[]
	symbols=[]
	for t in sequence:
		intervals.append( tuple([t[j] for j in [1,2]]) )
		colorVal = scalarMap.to_rgba( t[0] )
		symbols.append( colorVal )
	ax.broken_barh(intervals , (i, .8), facecolors=symbols)
	i+=1

ax.set_xlabel('seconds since start')
ax.grid(True)
plt.show()

#pat=QTIPrefixSpan(data2, 2)
#print pat

