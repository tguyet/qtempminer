/*
 * PythonFactory.cpp
 *
 *  Created on: Aug 7, 2015
 *      Author: tguyet
 */

#include <PythonModule/PythonFactory.h>
#include "QTI/pattern.h"

PythonFactory::PythonFactory() {
	// TODO Auto-generated constructor stub

}

PythonFactory::~PythonFactory() {
	// TODO Auto-generated destructor stub
}

vector<sequence> PythonFactory::createDatabase(PyObject *list) {
	vector<sequence> seqs;
	int n = PyList_Size(list);
	PyObject *item;
	int i;

	if (n < 0)
		return seqs; /* Not a list: error */

	//read the list
	for (i = 0; i < n; i++) {
		//cout << "read sequence " << i << endl;
		item = PyList_GetItem(list, i); /* Can't fail */
		//item is a list
		int nit = PyList_Size(item);
		if (nit > 0) { //a list
			sequence seq = createSequence(item);
			setId(seq,i);
			seqs.push_back(seq);
		} else {
			cout << "error: expected a list"<<endl;
		}
	}
	return seqs;
}

sequence PythonFactory::createSequence(PyObject *list) {
	int n = PyList_Size(list);
	PyObject *item;
	sequence seq;

	if (n < 0)
		return seq; /* Not a list: error */

	//read the list
	for (int i = 0; i < n; i++) {
		item = PyList_GetItem(list, i); /* Can't fail */
		event e=createEvent(item);
		seq.push_back(e);
	}
	return seq;
}


/**
 * TODO add the creation of the intervals
 */
PyObject* PythonFactory::createPattern(const pattern &pat) {
	//cout << "createPattern>> [" << pat << "]"<<endl;
    //creation du motif
    PyObject* PP = PyList_New( pat.size() );
    unsigned int i=0;
    const IntervalPatternFeature* fs=static_cast<const IntervalPatternFeature*>(pat.getFeature());
    for( i=0; i< pat.size(); i++ ) {
    	PyObject* Event;
    	if( fs ) {
    		Event = PyTuple_New( 3 );
    		PyTuple_SetItem(Event, 0, Py_BuildValue("l",pat.getSignature()[i]) );
    		PyTuple_SetItem(Event, 1, Py_BuildValue("f", (*fs)[i].s() ) );
    		PyTuple_SetItem(Event, 2, Py_BuildValue("f", (*fs)[i].f() ) );

    	} else {
    		Event = PyTuple_New( 1 );
    		PyTuple_SetItem(Event, 0, Py_BuildValue("l",pat.getSignature()[i]) );
    	}


    	//add the event to the list
        PyList_SET_ITEM(PP, i, Event );
    }
    PyObject* PySupport=Py_BuildValue("l",pat.support());
    PyObject* Pat=PyTuple_New(2);
    PyTuple_SET_ITEM(Pat, 0, PP );
    PyTuple_SET_ITEM(Pat, 1, PySupport );
    //cout << "<< createPattern"<<endl;
    return Pat;
}

/**
 * TODO add the creation of the intervals
 */
PyObject* PythonFactory::createPatternsList(const list<pattern*> &patterns) {
	//cout << "create patterns: "<< patterns.size() << endl;
    PyObject* pats = PyList_New( patterns.size() ); //-1 because the first pattern is empty !
    int i=0;
    for( auto p : patterns ) {
    	//cout << "test:" << *p <<endl;
    	//if( p.size() != 0) {
    		PyList_SET_ITEM(pats, i, createPattern(*p) );
    		i++;
    	/*} else {
    		cout << "empty"<<endl;
    	}*/
    }
    return pats;
}


/**
 * todo throw errors instead of messages !
 */
event PythonFactory::createEvent(PyObject *item){
	event e=sequenceFactory::createEvent();
	if( PyTuple_Check(item) ) {
		//item is a tuple
		int nit = PyTuple_Size(item);
		if( nit!=3 ) {
			cout << "error: expected tuple of length 3 (id, begin, end)."<<endl;
		} else {
			//read id
			long l=PyInt_AsLong( PyTuple_GetItem(item,0) );
			if( l<0 || l>INT_MAX) {// skip negative integers and too huge values
				cout << "error reading item: invalid integer value"<< endl;
			}
			setId(e, static_cast<unsigned int>(l));

			//read timestamps
			double s= PyFloat_AsDouble( PyTuple_GetItem(item,1) );
			double f= PyFloat_AsDouble( PyTuple_GetItem(item,2) );
			setFeature(e, new intervalFeature(s,f));
		}
	} else if( PyList_Check(item) ) {
		//item is a list
		int nit = PyList_Size(item);
		if( nit!=3 ) {
			cout << "error: expected tuple/list of length 3 (id, begin, end)."<<endl;
		} else {
			//read id
			long l=PyInt_AsLong( PyTuple_GetItem(item,0) );
			if( l<0 || l>INT_MAX) {// skip negative integers and too huge values
				cout << "error reading item: invalid integer value"<< endl;
			}
			setId(e, static_cast<unsigned int>(l));

			//read timestamps
			double s= PyFloat_AsDouble( PyTuple_GetItem(item,1) );
			double f= PyFloat_AsDouble( PyTuple_GetItem(item,2) );
			setFeature(e, new intervalFeature(s,f));
		}
	} else if( PyInt_Check(item) )  {
		//integer -> add a new itemset with one integer value
		long l=PyInt_AsLong(item);
		if( l<0 || l>INT_MAX) {// skip negative integers and too huge values
			cout << "error reading item: invalid integer value"<< endl;
		}
		setId(e, static_cast<unsigned int>(l));
	} else {
		cout << "error reading item: require integers"<< endl;
	}

	return e;
}
