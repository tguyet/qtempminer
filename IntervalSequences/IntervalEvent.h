/*
 * IntervalEvent.h
 *
 *  Created on: Aug 4, 2015
 *      Author: tguyet
 */

#ifndef INTERVALSEQUENCES_INTERVALEVENT_H_
#define INTERVALSEQUENCES_INTERVALEVENT_H_

#include "QTI/sequence.h"
#include "Interval.h"

class intervalFeature : public feature {
protected:
	//double _start=0, _finish=0;
	Interval I;
	static IntervalComparator * IntervalCmp; //TODO: use template and improve the general design !!
public:
	intervalFeature(){};
	intervalFeature(double s, double f);
	virtual ~intervalFeature(){};

	void set(double s, double f);

	double s() const {return I.l();};
	double f() const {return I.u();};
	double duration() const { return I.u()-I.l();};

	virtual bool operator==(const feature & f) const;
	virtual void print(ostream &os =cout) const;

	float operator-(const feature & f) const;
};

class IntervalSetComparator;

class intervalFeatureSet : public featureset {
protected:
	IntervalSetComparator *cmp;
public:
	intervalFeatureSet();
	intervalFeatureSet(const intervalFeatureSet &fs);

	virtual float operator-(const featureset &) const;
	virtual featureset *clone() const;
	virtual void append(feature *);

	intervalFeature & interval(unsigned int p);
	const intervalFeature & interval(unsigned int p) const;
	intervalFeature & operator[] (unsigned int p){return interval(p);};
	const intervalFeature & operator[](unsigned int p) const {return interval(p);};
};


class IntervalSetComparator {
public:
	virtual ~IntervalSetComparator(){};
	virtual double operator()(const intervalFeatureSet &I,const intervalFeatureSet &I2) =0;
};

class IntervalSetEuclidiean : public IntervalSetComparator {
public:
	double operator()(const intervalFeatureSet &I,const intervalFeatureSet &I2);
};


class CityBlock : public IntervalSetComparator {
public:
	double operator()(const intervalFeatureSet &I,const intervalFeatureSet &I2);
};

class Haussdorf : public IntervalSetComparator {
public:
	double operator()(const intervalFeatureSet &I,const intervalFeatureSet &I2);
};

class GowdaDiday : public IntervalSetComparator {
public:
	double operator()(const intervalFeatureSet &I,const intervalFeatureSet &I2);
};

class IntervalPatternFeature : public patternfeature {
protected:
	intervalFeatureSet *_fs;

public:
	/**
	 * local copy of the intervalfeatureSet
	 */
	IntervalPatternFeature(const intervalFeatureSet *f) {_fs = new intervalFeatureSet(*f);}

	/**
	 * hard copy of the intervalFeatureSet
	 */
	//IntervalPatternFeature(const IntervalPatternFeature &pf) {_fs = dynamic_cast<intervalFeatureSet *>( pf._fs->clone() );}


	/**
	 * hard copy of the intervalFeatureSet
	 */
	patternfeature *clone() const;

	const intervalFeature & operator[](unsigned int p) const {return (*_fs)[p];};

	/**
	 * destroy the intervalFeatureSet
	 */
	~IntervalPatternFeature() {delete(_fs);};
	void print(ostream& os) const;
private:
	IntervalPatternFeature():_fs(nullptr){};

	//const feature & operator[](unsigned int p) const {return (*_fs)[p];};
};

/**
 * \class IntervalSequenceFactory
 * TODO add the IntervalSetComparator in the factory constructor !
 */
class IntervalSequenceFactory: public sequenceFactory {
public:

	IntervalSequenceFactory():sequenceFactory(){eventdelimiter=';';};
	virtual ~IntervalSequenceFactory(){};

	event createEvent(istream &str) const;

	featureset *createFeature(event &e) const;
	featureset *createFeature(list<event*> &le) const;

	pattern * createPattern();

	patternfeature *createPatternFeature(const featureset *center,  list<const featureset*> &instances) const;
};

#endif /* INTERVALSEQUENCES_INTERVALEVENT_H_ */
